import * as rimraf from "rimraf";
import * as readdir from "recursive-readdir";
import { mdxDescriptionGen } from "./src/mdxDescriptionGen";
import { mdxCommandGen } from "./src/mdxCommandGen";
import { dirCommandRun } from "./src/constants";
import { promises as fsAsync } from "fs";
import { delay } from "./src/delay";

const removeDirCommandRun = async () => {
  try {
    await delay();
    await new Promise(resolve => rimraf(dirCommandRun, resolve));
    await delay();
  } catch (err) {
    console.error(err);
  }
};

const stageCommand = async (subCli: any, command: string) => {
  await removeDirCommandRun();
  await fsAsync.mkdir(dirCommandRun);
  process.chdir(dirCommandRun);
  await delay();
  await subCli.run(
    command
      .replace("[name]", "foo")
      .split(" ")
      .slice(2)
  );
  await delay();
  process.chdir("..");
};

const getSubClis = async (): Promise<string[]> => {
  const filePaths = await readdir("../lib/commands");
  return filePaths
    .filter(fp => fp.split(".")[fp.split(".").length - 1] === "js")
    .map(fp => fp.split("/")[fp.split("/").length - 1].replace(".js", ""))
    .filter(fp => fp.split(".")[fp.split(".").length - 1] !== "test");
};

const genDocs = async (subCliNames: string[]) => {
  for (const subCliName of subCliNames) {
    const subCli = require(`@commands/${subCliName}`).default;
    if (subCli.docs) {
      if (subCli.docs.description) {
        mdxDescriptionGen(subCliName, subCli.docs);
      }
      for (const c of subCli.docs.commands) {
        await stageCommand(subCli, c.command);
        await mdxCommandGen(subCliName, c.command, c.summary);
        await removeDirCommandRun();
      }
    }
  }
};

const main = async () => {
  const subClis = await getSubClis();
  genDocs(subClis);
};
main();
