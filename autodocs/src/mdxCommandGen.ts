import * as readdir from "recursive-readdir";
import { getTreeView } from "./treeView";
import { promises as fsAsync } from "fs";
import { getLeafOfPath, toKebabCase } from "./format";
import { getFrontMatter, getHeader, getSlug, getSummary } from "./mdxHelpers";
import { dirSort } from "./sort";

const getSortedFilePathsForDocumenting = async () => {
  let filePaths = await readdir("./.commandRun");
  filePaths = filePaths.sort(dirSort);
  return filePaths;
};

const getFileCodeSection = async (filePath: string) => {
  let result = "";
  const code = (await fsAsync.readFile(filePath, "utf-8")) as string;
  const leaf = getLeafOfPath(filePath);
  const dotLength = leaf.split(".").length;
  const extension = dotLength > 1 ? leaf.split(".")[dotLength - 1] : "";
  const id = toKebabCase(filePath.replace(".commandRun/", ""));
  const returnArrow = "[&uarr;](#top)";
  // result += `### ${returnArrow} ${leaf}{#${id}}\n\n`;
  result += `### ${returnArrow} ${leaf}\n\n`;
  result += "```" + extension + "\n";
  result += code.trim();
  result += "\n```\n\n";
  return result;
};

export const mdxCommandGen = async (subCliName: string, command: string, summary = "") => {
  const filePaths = await getSortedFilePathsForDocumenting();
  const frontMatter = getFrontMatter(subCliName, command);
  let data = frontMatter + "\n\n";
  data += getHeader("$ " + command) + "\n\n";
  data += getSummary(summary) + "\n\n";
  if (filePaths.length > 0) {
    data += getTreeView(filePaths) + "\n\n";
  }
  for (const fp of filePaths) {
    data += await getFileCodeSection(fp);
  }
  const slug = getSlug(command);
  await fsAsync.writeFile(`.docbuild/${slug}.mdx`, data, "utf-8");
};
