import { toKebabCase } from "./format";

export const getHeader = (title: string) => {
  return `# ${title}`;
};

export const getFrontMatter = (subCli: string, command: string) => {
  command = command.replace("tmpl", "");
  return `---
name: ${command}
route: /${subCli}/${toKebabCase(command)}
menu: ${subCli}
---`;
};

export const getSummary = (summary: string) => {
  if (summary) {
    summary = summary.replace(/  /, "\n>\n> ");
    return "> " + summary;
  }
  return "";
};

export const getSlug = (commandName: string) => toKebabCase(commandName.replace("tmpl ", ""));
