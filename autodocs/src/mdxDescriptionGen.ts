import { promises as fsAsync } from "fs";
import { getFrontMatter, getHeader, getSlug, getSummary } from "./mdxHelpers";


const getCommandInfo = async (subCliName: string, c: any) => {
  let result = "";
  result += c.summaryShort + `&nbsp;&nbsp;&nbsp;*[details...](${getSlug(c.command)})*`;
  result += "\n\n";
  result += "```bash\n";
  result += c.command + "\n";
  result += "```\n\n";
  return result;
};

export const mdxDescriptionGen = async (subCliName: string, subCliDocs: any, summary = "") => {
  const frontMatter = getFrontMatter(subCliName, "Summary");
  let data = frontMatter + "\n\n";
  data += getHeader(`Summary for *${subCliName}*`) + "\n\n";
  data += getSummary(subCliDocs.description) + "\n\n";
  for (const c of subCliDocs.commands) {
    data += await getCommandInfo(subCliName, c);
  }
  const slug = getSlug(`${subCliName}-${summary}`);
  await fsAsync.writeFile(`.docbuild/${slug}.mdx`, data, "utf-8");
};
