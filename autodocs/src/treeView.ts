import { getLeafOfPath, toKebabCase } from "./format";

interface IPathDatum {
  filePath: string;
  isFile: boolean;
}

const calcIndent = (filePath: string) => {
  const depth = filePath.split("/").length;
  let str = "└";
  for (let i = 0; i < depth; i++) {
    str += "─";
  }
  return str;
};
const deriveDirs = (filePath: string) => {
  return filePath.split("/").reduce((acc: string[], curr: string, index: number, arr: string[]) => {
    if (arr.length === 1) {
      return [];
    }
    if (index === 0) {
      return [...acc, curr];
      // return acc;
    } else if (index < arr.length - 1) {
      return [
        ...acc,
        (acc.join("/") + "/" + curr)
          .replace("src/src/", "src/")
          .replace("src/components/src/components/", "src/components/")
      ];
      // return acc;
    }
    return acc;
  }, []);
};

const dedupe = (pathData: IPathDatum[]) => {
  const dupe: string[] = [];
  return pathData.reduce((acc: IPathDatum[], curr: IPathDatum) => {
    if (dupe.indexOf(curr.filePath) === -1) {
      dupe.push(curr.filePath);
      acc.push(curr);
    }
    return acc;
  }, []);
};

const addDirsToFilePathList = (filePaths: IPathDatum[]) => {
  return filePaths.reduce((acc: IPathDatum[], pd: IPathDatum) => {
    const dirPathData = toPathData(deriveDirs(pd.filePath), false);
    acc = [...acc, ...dirPathData];
    acc.push(pd);
    return acc;
  }, [] as IPathDatum[]);
};

const toPathData = (filePaths: string[], isFile: boolean): IPathDatum[] =>
  filePaths.map(fp => ({ filePath: fp, isFile }));

export const getTreeView = (filePaths: string[]) => {
  filePaths = filePaths.map(fp => fp.replace(".commandRun/", ""));
  let pathData: IPathDatum[] = toPathData(filePaths, true);
  pathData = addDirsToFilePathList(pathData);
  pathData = dedupe(pathData);
  let result = "";
  for (const pd of pathData) {
    const leaf = getLeafOfPath(pd.filePath).replace(/^__/, "\\__");
    if (pd.isFile) {
      result += calcIndent(pd.filePath) + `[${leaf}](#${toKebabCase(pd.filePath)})` + "  \n";
    } else {
      result += calcIndent(pd.filePath) + leaf + "  \n";
    }
  }
  return "<h2>Files</h2>\n\nproject  \n" + result;
};
