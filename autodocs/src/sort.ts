export const dirSort = (a: string, b: string) => {
  const aDirs = a.split("/").length;
  const bDirs = b.split("/").length;
  if (aDirs > bDirs) {
    return -1;
  }
  if (aDirs < bDirs) {
    return 1;
  }
  if (a < b) {
    return -1;
  } else if (b < a) {
    return 1;
  }
  return 0;
};
