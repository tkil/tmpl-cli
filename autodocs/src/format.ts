export const toKebabCase = (str: string): string =>
  (str.match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g) || [])
    .map(c => c.toLowerCase())
    .join("-");

export const toProperCase = (str: string): string => str.slice(0, 1).toLocaleUpperCase() + str.slice(1, str.length);

export const getLeafOfPath = (filePath: string) => filePath.split("/")[filePath.split("/").length - 1];
