export const delay = (timeInMs = 100) => new Promise(resolve => setTimeout(resolve, timeInMs));
