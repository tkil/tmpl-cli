export default {
  base: "/tmpl-cli",
  title: "tmpl-cli",
  src: "./.docbuild",
  description: "Online docs for tmpl-cli",
  themeConfig: {
    initialColorMode: "dark"
  }
};
