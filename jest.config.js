module.exports = {
  clearMocks: true,
  collectCoverage: false,
  collectCoverageFrom: [
    // Match Pattern
    "**/*.{js,jsx,ts,tsx}",
    // Ignore Directories
    "!**.expected.**",
    "!**/__tests__/**",
    "!**/assets/**",
    "!**/bin/**",
    "!**/cli/**",
    "!**/coverage/**",
    "!**/dist/**",
    "!**/env/**",
    "!**/node_modules/**",
    "!**/tests/**",
    "!**/types/**",
    "!**/vendor/**",
    // Ignore Files
    "!**/**.config.**"
  ],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: ["/cli/", "/node_modules/"],
  globals: {
    "ts-jest": {
      tsConfig: "./tsconfig.json"
    }
  },
  moduleFileExtensions: ["js", "jsx", "ts", "tsx", "json"],
  globalSetup: "<rootDir>/src/testing/globalSetup.ts",
  globalTeardown: "<rootDir>/src/testing/globalTeardown.ts",
  moduleNameMapper: {
    "@env/(.*)": "<rootDir>/env/$1",
    "@commands/(.*)": "<rootDir>/src/commands/$1",
    "@engine/(.*)": "<rootDir>/src/engine/$1",
    "@generators/(.*)": "<rootDir>/src/generators/$1",
    "@modules/(.*)": "<rootDir>/node_modules/$1",
    "@templates/(.*)": "<rootDir>/src/templates/$1",
    "@testing/(.*)": "<rootDir>/src/testing/$1",
    "@utils/(.*)": "<rootDir>/src/utils/$1"
  },
  modulePathIgnorePatterns: [],
  testEnvironment: "node",
  testMatch: [
    "**/src/commands/__tests__/*test.+(js|jsx|ts|tsx)",
    "**/src/utils/__tests__/*test.+(js|jsx|ts|tsx)",
    "**/src/testing/__tests__/*test.+(js|jsx|ts|tsx)"
  ],
  testPathIgnorePatterns: ["/node_modules/", "}}"],
  testTimeout: 60 * 1000,
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest"
  }
};
