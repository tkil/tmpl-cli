tmpl-cli
========
[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@tkil/tmpl-cli.svg)](https://npmjs.org/package/tmpl-cli)
[![Downloads/week](https://img.shields.io/npm/dw/@tkil/tmpl-cli.svg)](https://npmjs.org/package/tmpl-cli)
[![License](https://img.shields.io/npm/l/@tkil/tmpl-cli.svg)](https://github.com/tmp/tmpl-cli/blob/master/package.json)

Table of Contents
=================

<!-- toc -->
* [Install](#install)
* [Summary](#summary)
* [Online Docs](#online-docs)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->


# Install
```
npm install -g @tkil/tmpl-cli
```

# Summary
`tmpl` is a cli tool for generating boiler plate code and spiking projects.  Its toolset is geared toward a holistic frontend developer.  It is a heavy cli consisting of many sub-clis.  Each sub-cli governs a category such as `web`, `react`, `vue`, etc...  A comprehensive list can be found in this readme under [Commands](#commands) or [Online Docs](#online-docs).  The online docs may also be summoned locally anytime once installed via the command `tmpl docs` from the terminal.

Background / Inspiration  
`tmpl` was built using [OCLIF](https://oclif.io/).  It takes inspiration from [Yeoman](https://yeoman.io/) and the [Angular CLI](https://cli.angular.io/).  To my knowledge, the best thing React has going for it is [create-react-app](https://github.com/facebook/create-react-app).  I feel the React community could have done better for us.  This tool was born out of envy for Angular CLI's ability to speed up development and deliver an awesome coding experience.

Future  
The project started with a React focus, but it will become more and more of an all purpose template and code generation tool as it grows and matures. This will range from generating web components to creating common config files. Requests are welcome, but I treat this project as a personal tool that I make public and share with others.  That means the code generated and `tmpl`'s toolset can be somewhat opinionated.

There is currently no way to configure `tmpl` for custom needs. I am open to building this in if enough people start using it and that becomes an ask.

Note: the docs are kept up to date using automation for both this readme and the online docs.

# Online Docs
Documentation: https://tkil.gitlab.io/tmpl-cli/

*Docs are powered by [Docz](https://www.docz.site/)*


# Usage
<!-- usage -->
```sh-session
$ npm install -g tmpl-cli
$ tmpl COMMAND
running command...
$ tmpl (-v|--version|version)
tmpl-cli/1.0.3 darwin-x64 node-v15.10.0
$ tmpl --help [COMMAND]
USAGE
  $ tmpl COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`tmpl api [NAME]`](#tmpl-api-name)
* [`tmpl cypress [COMMAND] [ARG]`](#tmpl-cypress-command-arg)
* [`tmpl docs`](#tmpl-docs)
* [`tmpl env [NAME]`](#tmpl-env-name)
* [`tmpl gatsby [COMMAND] [ARG]`](#tmpl-gatsby-command-arg)
* [`tmpl git [COMMAND]`](#tmpl-git-command)
* [`tmpl hbs [COMMAND] [ARG]`](#tmpl-hbs-command-arg)
* [`tmpl help [COMMAND]`](#tmpl-help-command)
* [`tmpl html [COMMAND] [NAME]`](#tmpl-html-command-name)
* [`tmpl husky [COMMAND] [ARG]`](#tmpl-husky-command-arg)
* [`tmpl jest [COMMAND] [ARG]`](#tmpl-jest-command-arg)
* [`tmpl lintstaged [COMMAND] [ARG]`](#tmpl-lintstaged-command-arg)
* [`tmpl lisp`](#tmpl-lisp)
* [`tmpl node [COMMAND] [ARG]`](#tmpl-node-command-arg)
* [`tmpl prettier [COMMAND] [ARG]`](#tmpl-prettier-command-arg)
* [`tmpl python`](#tmpl-python)
* [`tmpl react [COMMAND] [ARG]`](#tmpl-react-command-arg)
* [`tmpl readme`](#tmpl-readme)
* [`tmpl schema [NAME]`](#tmpl-schema-name)
* [`tmpl style [COMMAND] [ARG]`](#tmpl-style-command-arg)
* [`tmpl testing [COMMAND] [ARG]`](#tmpl-testing-command-arg)
* [`tmpl util [COMMAND] [ARG]`](#tmpl-util-command-arg)
* [`tmpl vue [COMMAND] [ARG]`](#tmpl-vue-command-arg)
* [`tmpl web [COMMAND] [ARG]`](#tmpl-web-command-arg)
* [`tmpl webpack [COMMAND] [ARG]`](#tmpl-webpack-command-arg)

## `tmpl api [NAME]`

Sub cli for api generator

```
Sub cli for api generator

USAGE
  $ tmpl api [NAME]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLES
  Create an api call helper
  $ tmpl api [name]

  Create an api call helper, force ts
  $ tmpl api [name] -t
```


## `tmpl cypress [COMMAND] [ARG]`

Sub cli for Cypress templates

```
Sub cli for Cypress templates

USAGE
  $ tmpl cypress [COMMAND] [ARG]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLES
  Inits Cypress
  $ tmpl cypress init

  Adds axe A11y
  $ tmpl cypress axe
```


## `tmpl docs`

Sub cli for tmpl's docs

```
Sub cli for tmpl's docs

USAGE
  $ tmpl docs

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help

EXAMPLE
  Open tmpl's docs in a browser
  $ tmpl docs
```


## `tmpl env [NAME]`

Sub cli for env generator

```
Sub cli for env generator

USAGE
  $ tmpl env [NAME]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLE
  Create env files under /env with config
  $ tmpl env
```


## `tmpl gatsby [COMMAND] [ARG]`

Sub cli for Gatsby project init and generators

```
Sub cli for Gatsby project init and generators

USAGE
  $ tmpl gatsby [COMMAND] [ARG]

OPTIONS
  -c, --contextName=contextName
  -d, --dry-run                  preview install with no changes
  -f, --force                    force install and overwrite
  -h, --help                     show CLI help
  -i, --install                  Active install instead of dep add
  -j, --javascript               force javascript templates
  -m, --mobxName=mobxName
  -t, --typescript               force typescript templates

EXAMPLE
  Create a new Gatsby project
  $ tmpl react init
```


## `tmpl git [COMMAND]`

Sub Cli to Create gitignores and init commits

```
Sub Cli to Create gitignores and init commits

USAGE
  $ tmpl git [COMMAND]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLES
  Creates a general .gitignore and inits
  $ tmpl git

  Creates a csharp .gitignore and inits
  $ tmpl git csharp
```


## `tmpl hbs [COMMAND] [ARG]`

Creates a new hbs project

```
Creates a new hbs project

USAGE
  $ tmpl hbs [COMMAND] [ARG]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLE
  Inits new hbs project
  $ tmpl hbs init
```


## `tmpl help [COMMAND]`

display help for tmpl

```
display help for <%= config.bin %>

USAGE
  $ tmpl help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```


## `tmpl html [COMMAND] [NAME]`

Sub cli for html templates

```
Sub cli for html templates

USAGE
  $ tmpl html [COMMAND] [NAME]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help

EXAMPLE
  Adds a simple html form file
  $ tmpl html form [name]
```


## `tmpl husky [COMMAND] [ARG]`

Sub cli for Husky install

```
Sub cli for Husky install

USAGE
  $ tmpl husky [COMMAND] [ARG]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLE
  Creates a .huskyrc and installs
  $ tmpl husky
```


## `tmpl jest [COMMAND] [ARG]`

Sub cli for jest install

```
Sub cli for jest install

USAGE
  $ tmpl jest [COMMAND] [ARG]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLES
  Creates a jest.config.js and installs
  $ tmpl jest

  Creates a jest.config.js and installs
  $ tmpl jest -t
```


## `tmpl lintstaged [COMMAND] [ARG]`

Sub cli for lint-staged templates

```
Sub cli for lint-staged templates

USAGE
  $ tmpl lintstaged [COMMAND] [ARG]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLE
  Creates a .lintstagedrc.jso file and installs
  $ tmpl lintstaged
```


## `tmpl lisp`

Creates a small Lisp starter project

```
Creates a small Lisp starter project

USAGE
  $ tmpl lisp

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help

EXAMPLE
  Creates a small Lisp starter project
  $ tmpl lisp
```


## `tmpl node [COMMAND] [ARG]`

Sub cli for Node project generation

```
Sub cli for Node project generation

USAGE
  $ tmpl node [COMMAND] [ARG]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLES
  Initialize a node project
  $ tmpl node init

  Initialize a node project
  $ tmpl node init -t
```


## `tmpl prettier [COMMAND] [ARG]`

Sub cli for prettier

```
Sub cli for prettier

USAGE
  $ tmpl prettier [COMMAND] [ARG]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add
  -p, --peer

EXAMPLES
  Installs prettier with eslint bridge to vscode
  $ tmpl prettier

  Installs prettier, eslint bridge, lint-staged and husky
  $ tmpl prettier -p
```


## `tmpl python`

Creates a small Python starter project

```
Creates a small Python starter project

USAGE
  $ tmpl python

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help

EXAMPLE
  Creates a small Python starter project
  $ tmpl python
```


## `tmpl react [COMMAND] [ARG]`

Sub cli for React project init and generators

```
Sub cli for React project init and generators

USAGE
  $ tmpl react [COMMAND] [ARG]

OPTIONS
  -c, --contextName=contextName
  -d, --dry-run                  preview install with no changes
  -f, --force                    force install and overwrite
  -h, --help                     show CLI help
  -i, --install                  Active install instead of dep add
  -j, --javascript               force javascript templates
  -m, --mobxName=mobxName
  -q, --isQuery
  -t, --typescript               force typescript templates

EXAMPLES
  Create a new React project
  $ tmpl react init

  Create a functional component
  $ tmpl react fc [name]

  Create a functional component, force Typescript
  $ tmpl react fc [name] -t

  Create a functional component with context connected
  $ tmpl react fc [name] -c contextName

  Create a functional component with mobx store connection
  $ tmpl react fc [name] -m mobxStoreName

  Create a react data context under src/stores
  $ tmpl react context [name]

  Create a mobx state tree model under src/stores
  $ tmpl react mst [name]
```


## `tmpl readme`

Creates a README.md file in the current directory

```
Creates a README.md file in the current directory

USAGE
  $ tmpl readme

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLE
  Creates a README.md file in the current directory
  $ tmpl readme
```


## `tmpl schema [NAME]`

Sub cli for schema generator

```
Sub cli for schema generator

USAGE
  $ tmpl schema [NAME]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLE
  Creates a schema
  $ tmpl schema [name]
```


## `tmpl style [COMMAND] [ARG]`

Sub cli for Style templates

```
Sub cli for Style templates

USAGE
  $ tmpl style [COMMAND] [ARG]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLES
  Creates a css colors file
  $ tmpl style colors

  Creates a css colors-tailwind file
  $ tmpl style colors-tailwind

  Creates a css reset file
  $ tmpl style reset

  Creates a css file to import sakura
  $ tmpl style sakura

  Creates a css file to import water
  $ tmpl style water
```


## `tmpl testing [COMMAND] [ARG]`

Sub cli for testing templates

```
Sub cli for testing templates

USAGE
  $ tmpl testing [COMMAND] [ARG]

OPTIONS
  -d, --dry-run  preview install with no changes
  -f, --force    force install and overwrite
  -h, --help     show CLI help
  -i, --install  Active install instead of dep add

EXAMPLES
  Creates device test helpers
  $ tmpl testing device

  Creates dom test helpers
  $ tmpl testing dom

  Adds jest-dom to project
  $ tmpl testing jest-dom

  Creates location test helpers
  $ tmpl testing location

  Creates stub files for test runner
  $ tmpl testing stubs
```


## `tmpl util [COMMAND] [ARG]`

Sub cli for util helps

```
Sub cli for util helps

USAGE
  $ tmpl util [COMMAND] [ARG]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLES
  Adds async util
  $ tmpl util async

  Adds cookie util
  $ tmpl util cookie

  Adds dynamic util
  $ tmpl util dynamic

  Adds event-listener util
  $ tmpl util event-listener

  Adds history util
  $ tmpl util history

  Adds location util
  $ tmpl util location

  Adds polyfills util
  $ tmpl util polyfills

  Adds react util
  $ tmpl util react

  Adds retry util
  $ tmpl util retry
```


## `tmpl vue [COMMAND] [ARG]`

Sub cli for Vue project init and generators

```
Sub cli for Vue project init and generators

USAGE
  $ tmpl vue [COMMAND] [ARG]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLES
  Create a new Vue project
  $ tmpl vue init

  Create a vue component
  $ tmpl vue component [name]
```


## `tmpl web [COMMAND] [ARG]`

Sub cli for vanilla web projects

```
Sub cli for vanilla web projects

USAGE
  $ tmpl web [COMMAND] [ARG]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLES
  Inits new web project
  $ tmpl web init

  Creates a web component
  $ tmpl web component [name]
```


## `tmpl webpack [COMMAND] [ARG]`

Creates webpack.config.js and installs deps

```
Creates webpack.config.js and installs deps

USAGE
  $ tmpl webpack [COMMAND] [ARG]

OPTIONS
  -d, --dry-run     preview install with no changes
  -f, --force       force install and overwrite
  -h, --help        show CLI help
  -i, --install     Active install instead of dep add
  -j, --javascript  force javascript templates
  -t, --typescript  force typescript templates

EXAMPLE
  $ tmpl webpack
```

<!-- commandsstop -->

### Version History
0.0.x - Pre-release, assume unstable
