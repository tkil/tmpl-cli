declare module "./templateEngine";

export interface IDocs {
  commands: IDocCommand[];
  description: string;
  descriptionShort: string;
}

export interface ICommand {
  args: IArgs;
  flags: IFlags;
}
export interface IArgs {
  [key: string]: string;
}

export interface IFlags {
  force: boolean;
  install?: boolean;
  "dry-run"?: boolean;
  javascript?: boolean;
  typescript?: boolean;
}

export interface IDocCommand {
  command: string;
  summary: string;
  summaryShort: string;
}

export interface IObjString {
  [key: string]: string;
}

export interface IObjAny {
  [key: string]: any;
}

export interface IGeneratorResult {
  files: any[][];
  devPackages: string[];
  prodPackages: string[];
}
