const path = require("path");
const readDir = require("recursive-readdir");
const tsConfig = require("../tsconfig.json");
const { readFileSync, writeFileSync } = require("fs");

const dir = path.resolve(__dirname, "..");
const libDir = path.resolve(dir, "lib");

const adjustPath = p => path.resolve(dir, p.replace("/src", "/lib")).replace("/*", "/");

const derivePathMap = tsConfigPath => {
  return Object.keys(tsConfigPath).map(key => {
    return [`"${key}`.replace("/*", "/"), `${adjustPath(tsConfigPath[key][0])}`];
  });
};

const getFileDepth = filePath => filePath.replace(libDir, "").split("/").length - 2;

const makeUpDirs = num => {
  let subPath = ".";
  for (let i = 0; i < num; i++) {
    subPath = subPath + "/..";
  }
  return subPath;
};

const updateFileDataUsingPathMap = (fileData, filePath, pathMap) => {
  let data = fileData;
  for (const pm of pathMap) {
    const upDirs = makeUpDirs(getFileDepth(filePath));
    const newPath = '"' + upDirs + pm[1].replace(libDir, "");
    data = data.replace(new RegExp(pm[0], "g"), newPath);
  }
  return data;
};

const main = async () => {
  // const tsConfigData = JSON.parse(tsConfig);
  const pathMap = derivePathMap(tsConfig.compilerOptions.paths);
  const filePaths = await readDir(path.resolve(dir, "lib"));
  for (const fp of filePaths) {
    let data = readFileSync(fp, "utf-8");
    data = updateFileDataUsingPathMap(data, fp, pathMap);
    writeFileSync(fp, data, "utf-8");
  }
};
main();
