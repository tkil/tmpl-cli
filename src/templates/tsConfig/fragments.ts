export const tsConfigJsxReact = `
    "jsx": "react",`;

export const tsConfigFilesVueShim = `
    "files": [
        "./types/vue-shims.d.ts"
    ],`;
