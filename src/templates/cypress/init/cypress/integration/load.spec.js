/// <reference types="cypress" />

const url = "https://example.cypress.io/commands/window";

context("Window", () => {
  beforeEach(() => {
    cy.visit(url);
  });

  it("cy.title() - get the title", () => {
    // https://on.cypress.io/title
    cy.title().should("include", "Kitchen Sink");
  });
});
