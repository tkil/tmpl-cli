/// <reference types="cypress" />

const url = "https://example.cypress.io/commands/window";

context("Accessibility", () => {
  it("Has no detectable a11y violations on load for mobile", () => {
    cy.viewport("iphone-6");
    cy.visit(url);
    cy.injectAxe();
    cy.checkA11y();
  });

  it("Has no detectable a11y violations on load for desktop", () => {
    cy.viewport("macbook-16");
    cy.visit(url);
    cy.injectAxe();
    cy.checkA11y();
  });
});
