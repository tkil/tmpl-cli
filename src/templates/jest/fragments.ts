export const jestTypescriptGlobals = `
  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.json"
    }
  },`;

export const jestTransformTypescript = `
  "^.+\\\\.tsx?$": "ts-jest",`;

export const jestTransformJavascript = `
  "^.+\\\\.jsx?$": "babel-jest",`;

export const jestTransformVue = `
  ".*\\\\.(vue)$": "vue-jest",`;
