(in-package #:cl-user)
(defpackage #:main
  (:use #:cl)
  (:export #:greet))
(in-package #:main)

(defun greet ()
  "Hello, World!"
)

(defun main ()
  (print (greet))
)

(main)
