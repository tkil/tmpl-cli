;;; 
;;; main v1.1.0
;;; 
(ql:quickload "lisp-unit")
#-xlisp-test (load "main")

(defpackage #:main-test
  (:use #:common-lisp #:lisp-unit))
(in-package #:main-test)

(define-test
  say-hello!
  (assert-equal
    "Hello, World!"
    (main:greet)))

#-xlisp-test
(let ((*print-errors* t)
      (*print-failures* t))
  (run-tests :all))
