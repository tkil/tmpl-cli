export const webpackSassLoaderRule = `,
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }`;

export const webpackTypescriptResolvePaths = `,
  resolve: {
    mainFields: ['browser', 'main', 'module'],
    extensions: ['.ts', '.tsx', '.js', '.js', '.json'],
    plugins: [new TsconfigPathsPlugin({ configFile: path.join(dir, 'tsconfig.json') })]
  }`;

export const webpackTsconfigPathsPlugin = 'const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");';
export const webpackTsLoaderRule = `
      {
        test: /\.(ts|tsx)$/,
        use: 'ts-loader',
        include: dir,
        exclude: [nodeModulesDir, /.test.tsx?$/]
      },`;
