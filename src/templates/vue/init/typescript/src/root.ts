import Vue from "vue";

export const initVue = () => {
  const rootId = "root";
  const rootElement = document.getElementById(rootId);
  if (rootElement) {
    new Vue({
      el: `#${rootId}`,
      data: {
        message: "Vue working"
      }
    })
  } else {
    console.error(`Could not find element with ID: ${rootId}`);
  }
};
