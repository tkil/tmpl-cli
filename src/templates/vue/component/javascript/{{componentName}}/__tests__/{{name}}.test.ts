import { shallowMount } from '@vue/test-utils'
import {{nameProperCase}} from '../{{name}}.vue'

describe("{{nameProperCase}} component", () => {
  describe("render", () => {
    it("should render without error", () => {
      // Arrange
      const wrapper = shallowMount({{nameProperCase}})
      // Act
      const element = wrapper.element.firstChild
      // Assert
      expect(element).not.toBeNull();
    })
  })
})