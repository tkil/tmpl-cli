const path = require("path");
const dotenv = require("dotenv");
const level = (process.env.LEVEL || process.env.PROMOTION_LEVEL || "LOCAL").toLocaleUpperCase();
const region = (process.env.REGION || "us-east-1").toLocaleLowerCase();

let envFile = ".env.local";

switch (level) {
  case "DEVELOPMENT":
    envFile = ".env.development";
    break;
  case "STAGING":
    envFile = ".env.staging";
    break;
  case "PRODUCTION":
    envFile = ".env.production";
    break;
}

const envPath = path.join(__dirname, envFile);
const config = () => dotenv.config({ path: envPath });

module.exports = {
  config,
  envPath,
  level,
  region
};
