export const fetch{{nameProperCase}}Origin = "https://some-domain.com";

interface IDataType {
  id: number;
  value: string;
}

const isFetchedDataValid = (body: string): boolean => {
  const json = JSON.parse(body);
  if (
    //
    typeof json.id !== "number" ||
    typeof json.value !== "string"
  ) {
    return false;
  }
  return true;
};

const transformAndMapFetchedData = (body: string): IDataType => {
  const json = JSON.parse(body);
  return {
    id: json.id,
    value: json.value
  };
};

export const fetch{{nameProperCase}} = async (inquiry: string) => {
  try {
    const response = await fetch(`${fetch{{nameProperCase}}Origin}/endpoint`, {
      method: "post",
      body: JSON.stringify({ inquiry: inquiry }),
      headers: { "Content-Type": "application/json" }
    });
    const body = await response.text();
    if (response.status >= 400) {
      throw Error(`Error talking to service - ${response.status} - ${body}`);
    }
    if (!isFetchedDataValid(body)) {
      console.error("Data from server is invalid");
      return { data: undefined, status: "invalid" };
    }
    return {
      data: transformAndMapFetchedData(body),
      status: "success"
    };
  } catch (err) {
    console.error(err);
    return { data: undefined, status: "error" };
  }
};
