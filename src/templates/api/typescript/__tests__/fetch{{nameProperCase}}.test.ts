import nock from "nock";
import { fetch{{nameProperCase}}, fetch{{nameProperCase}}Origin } from "../fetch{{nameProperCase}}";

describe("API: fetch{{nameProperCase}}", () => {
  it("should have success status with expected data", async () => {
    // Arrange
    const mockRequest = { inquiry: "input" };
    const mockResponse = { id: 1, value: "foobar" };
    const expectedDataResult = mockResponse; // Happens to be the same as the data is the same shape
    nock(fetch{{nameProperCase}}Origin)
      .post("/endpoint", mockRequest)
      .reply(200, mockResponse);
    // Act
    const result = await fetch{{nameProperCase}}("input");
    // Assert
    expect(result.status).toEqual("success");
    expect(result.data).toEqual(expectedDataResult);
  });

  it("should have invalid status for invalid data", async () => {
    // Arrange
    const errorRestore = console.error;
    console.error = () => {};
    const mockRequest = { inquiry: "input" };
    const mockResponse = { id: 1 };
    nock(fetch{{nameProperCase}}Origin)
      .post("/endpoint", mockRequest)
      .reply(200, mockResponse);
    // Act
    const result = await fetch{{nameProperCase}}("input");
    // Assert
    expect(result.status).toEqual("invalid");
    expect(result.data).toBeUndefined();
    // Cleanup
    console.error = errorRestore;
  });

  it("should have error status with undefined data", async () => {
    // Arrange
    const errorRestore = console.error;
    console.error = () => {};
    const mockRequest = { inquiry: "input" };
    nock(fetch{{nameProperCase}}Origin)
      .post("/endpoint", mockRequest)
      .reply(500);
    // Act
    const result = await fetch{{nameProperCase}}("input");
    // Assert
    expect(result.status).toEqual("error");
    expect(result.data).toBeUndefined();
    // Cleanup
    console.error = errorRestore;
  });
});
