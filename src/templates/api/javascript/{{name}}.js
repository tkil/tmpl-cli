import Schema from "validate";

export const {{name}}Domain = "https://some-domain.com";

const dataSchema = new Schema({
  id: { "type": Number, required: true },
  value: { "type": String, required: true }
})

export const fetchResourceValidate = (response) => {
  const unknown = await response.json();
  if (dataSchema.validate(unknown).length > 0) {
    return { data: undefined, status: "INVALID" };
  }
  return { data: unknown, status: "SUCCESS" };
}

export const fetchResource = async (payload) => {
  try {
    const response = await fetch(`${ {{name}}Domain}/endpoint`, {
      method: "post",
      body: JSON.stringify(payload),
      headers: { "Content-Type": "application/json" }
    });
    if (response.statusCode >= 400) {
      const body = await response.text();
      throw Error(`Error talking to service - ${response.statusCode} - ${body});
    }
    return fetchResourceValidate(response);
  } catch (err) {
    console.error(err);
    return { data: undefined, status: "ERROR" };
  }
};
