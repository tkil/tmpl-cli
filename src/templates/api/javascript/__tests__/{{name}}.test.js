// Recomendation: Wire node-fetch into your jest setup
import fetch from "node-fetch";
//@ts-ignore
window.fetch = fetch;

import nock from "nock";
import { fetchResource, {{name}}Domain } from "../{{name}}";


describe("API {{name}}", () => {
  describe("fetchData()", () => {
    it("should have success status with expected data", async () => {
      // Arrange
      const mockRequest = { some: "input" };
      const mockResponse = { id: 1, value: "foobar" };
      nock({{name}}Domain)
        .post("/endpoint", mockRequest)
        .reply(200, mockResponse);
      // Act
      const result = await fetchResource(mockRequest);
      // Assert
      expect(result.status).toEqual("SUCCESS");
      expect(result.data).toEqual(mockResponse);
    });
    it("should have invalid status for invalid data", async () => {
      // Arrange
      const mockRequest = { some: "input" };
      const mockResponse = { id: 1 };
      nock({{name}}Domain)
        .post("/endpoint", mockRequest)
        .reply(200, mockResponse);
      // Act
      const result = await fetchResource(mockRequest);
      // Assert
      expect(result.status).toEqual("INVALID");
      expect(result.data).toBeUndefined();
    });
    it("should have error status with undefined data", async () => {
      // Arrange
      const errorRestore = console.error;
      console.error = () => {};
      const mockRequest = { some: "input" };
      nock({{name}}Domain)
        .post("/endpoint", mockRequest)
        .reply(500);
      // Act
      const result = await fetchResource(mockRequest);
      // Assert
      expect(result.status).toEqual("ERROR");
      expect(result.data).toBeUndefined();
      // Cleanup
      console.error = errorRestore;
    });
  });
});
