/**
 * @typedef {Object} IRenderResult
 * @property {HTMLElement} baseElement - __PROPERTY_DESCRIPTION__
 * @return {IRenderResult}
 */
export const render = html => {
  document.body.innerHTML = html;
  return {
    baseElement: document.body.firstChild
  };
};

/**
 * @return {void}
 */
export const clearDom = () => (document.body.innerHTML = "");

/**
 * @param {HTMLElement} el
 * @param {string} type
 * @return {boolean}
 */
export const hasChildElementType = (el, type) => {
  for (const child of el.childNodes) {
    if (child.tagName === type.toUpperCase()) {
      return true;
    }
  }
  return false;
};
