export const render = (html: string) => {
  document.body.innerHTML = html;
  return {
    baseElement: document.body.firstChild
  };
};

export const clearDom = () => (document.body.innerHTML = "");

export const hasChildElementType = (el: HTMLElement, type: string) => {
  for (const child of el.childNodes) {
    if ((child as HTMLElement).tagName === type.toUpperCase()) {
      return true;
    }
  }
  return false;
};
