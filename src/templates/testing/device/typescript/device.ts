//@ts-ignore
export const setScreenWidth = (width: number) => (window.innerWidth = width);

export const setMobile = () => setScreenWidth(400);

export const setDesktop = () => setScreenWidth(960);
