/**
 * @return {void}
 */
export const setScreenWidth = width => (window.innerWidth = width);

/**
 * @return {void}
 */
export const setMobile = () => setScreenWidth(400);

/**
 * @return {void}
 */
export const setDesktop = () => setScreenWidth(960);
