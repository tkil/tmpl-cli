import { replaceAndMockLocation, restoreWindowLocation } from "../location";

describe("Testing Util: Location", () => {
  describe("replaceAndMockLocation()", () => {
    const testUrl = "https://my-url.com:443/questions/thing?qry=thing#123";
    const { location } = window;

    beforeAll(() => {
      replaceAndMockLocation();
    });

    afterAll(() => {
      window.location = location;
    });

    it("should handle assignments to location.href correctly", () => {
      window.location.href = testUrl;
      expect(window.location.href).toEqual(testUrl);
      expect(window.location.hrefSetSpy).toBeCalledTimes(1);
    });

    it("should get the correct href", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.href).toEqual(testUrl);
    });

    it("should get the correct hash", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.hash).toEqual("#123");
    });

    it("should get the correct host", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.host).toEqual("my-url.com:443");
    });

    it("should get the correct hostname", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.hostname).toEqual("my-url.com");
    });

    it("should get the correct origin", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.origin).toEqual("https://my-url.com:443");
    });

    it("should get the correct pathname", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.pathname).toEqual(
        "/questions/43697455/how-to-mock-replace-getter-function-of-object-with-jest"
      );
    });

    it("should get the correct port", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.port).toEqual("443");
    });

    it("should get the correct protocol", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.protocol).toEqual("https:");
    });

    it("should get the correct search", () => {
      window.location.hrefInit = testUrl;
      expect(window.location.search).toEqual("?qry=thing#123");
    });
  });

  describe("restoreWindowLocation", () => {
    it("should restore window.location when restore is invoked", () => {
      // Arrange
      const originalHref = window.location.href;
      // Act
      replaceAndMockLocation("https://www.fake.com");
      const mockedHref = window.location.href;
      restoreWindowLocation();
      // Assert
      expect(window.location.href).toEqual(originalHref);
    });

    it("Using replaceAndMockLocation multiple times should not destroy window.location", () => {
      // Arrange
      const originalHref = window.location.href;
      // Act
      replaceAndMockLocation("https://www.fake.com");
      replaceAndMockLocation("https://www.fake.com");
      replaceAndMockLocation("https://www.fake.com");
      const mockedHref = window.location.href;
      restoreWindowLocation();
      // Assert
      expect(window.location.href).toEqual(originalHref);
    });
  });
});
