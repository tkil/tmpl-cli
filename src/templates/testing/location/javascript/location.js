let windowLocationActual = window.location;

export const replaceAndMockLocation = (initialUrl = "") => {
  // State
  let _url = "";

  const location = {
    set hrefInit(url) {
      _url = url.toString();
    },
    hrefSetSpy: jest.fn(),
    get href() {
      return _url;
    },
    set href(url) {
      location.hrefSetSpy(url);
      _url = url.toString();
    },
    get hash() {
      return `#${_url.replace(/^.*\#/, "")}`;
    },
    get host() {
      return location.origin.replace(/.*\/\//, "");
    },
    get hostname() {
      return location.host.replace(/:.*/, "");
    },
    get origin() {
      const defaultProtocol = "http:";
      const protocol = _url.replace(/\/\/.*/, "");
      const address = _url.replace(/.*\/\//, "");
      return `${protocol || defaultProtocol}//${address.replace(/\/.*/, "")}`;
    },
    get pathname() {
      return _url.replace(location.origin, "").replace(/\?.*/, "");
    },
    get port() {
      return _url.replace(/.*:/, "").replace(/\/.*/, "");
    },
    get protocol() {
      return _url.replace(/\/\/.*/, "");
    },
    get search() {
      return `?${_url.replace(/.*\?/, "")}`;
    },
  };

  // Overwrite window.location and preserve actual
  _url = initialUrl || window.location.href;
  if (!windowLocationActual) {
    windowLocationActual = window.location;
  }
  delete window.location;
  window.location = location;
};

export const restoreWindowLocation = () => {
  window.location = windowLocationActual;
  windowLocationActual = undefined;
};
