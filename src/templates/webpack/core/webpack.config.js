{{preConfig}}

// Main Webpack Config
module.exports = {
  target: {{target}},
  mode: webpackMode,
  entry: {{entry}},{{devServer}}
  output: {{output}},
  resolve: {{resolve}},
  module: {
    rules: [{{loaders}}]
  },
  plugins: {{plugins}}
};
