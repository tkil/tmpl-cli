import Schema from "validate";

export const {{schemaName}} = new Schema({
  id: { "type": Number, required: true },
  value: { "type": String, required: true }
})

export default {
  {{schemaName}}
}