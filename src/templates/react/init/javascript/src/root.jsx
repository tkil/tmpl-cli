import React from "react";
import { render } from "react-dom";

export const initReact = () => {
  const root = "root";
  const rootElement = document.getElementById(root);
  if (rootElement) {
    render(<div>React Working</div>, rootElement);
  } else {
    console.error(`Could not find element with ID: ${root}`);
  }
};
