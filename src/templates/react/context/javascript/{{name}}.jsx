import React, { createContext, useState } from "react";

// Init
export const {{name}}ContextEmptyState = {
  value: "",
};

const emptyAction = () => {};
export const {{name}}ContextEmptyActions = {
  setValue: () => emptyAction,
};

export const {{nameProperCase}}Context = createContext({
  state: {{name}}ContextEmptyState,
  actions: {{name}}ContextEmptyActions,
});

export const {{nameProperCase}}ContextConsumer = {{nameProperCase}}Context.Consumer;

// Implementation
export const {{nameProperCase}}ContextProvider = ({ children, initialState }) => {
  const [state, updateState] = useState(initialState);

  const actions = {
    setValue: (val) => {
      updateState({ ...state, value: val });
    },
  };

  return <{{nameProperCase}}Context.Provider value={{{{ actions, state }}}}>{children}</{{nameProperCase}}Context.Provider>;
};

export default { {{nameProperCase}}Context, {{nameProperCase}}ContextProvider };
