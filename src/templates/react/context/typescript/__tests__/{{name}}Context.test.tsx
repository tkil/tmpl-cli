import React from "react"
import { fireEvent, render } from "@testing-library/react"

import { {{nameProperCase}}Context, {{nameProperCase}}ContextProvider, I{{nameProperCase}}Context } from "../{{name}}Context"


describe("{{nameProperCase}}Context", () => {
  describe("setValue()", () => {
    it("should set value to foo", () => {
      // Arrange
      const { getByTestId } = render(
        <{{nameProperCase}}ContextProvider>
          <{{nameProperCase}}Context.Consumer>
            {
              ({state, actions}: I{{nameProperCase}}Context) => {
                return <>
                  <span data-testid="action" onClick={() => actions.setValue("foo")}></span>
                  <span data-testid="result">{state.value}</span>
                </>
              }
            }
          </{{nameProperCase}}Context.Consumer>
        </{{nameProperCase}}ContextProvider>
      )
      // Act
      fireEvent.click(getByTestId("action"))
      const actual = getByTestId("result").textContent
      // Assert
      const expected = "foo"
      expect(actual).toEqual(expected)
    })
  })
})
