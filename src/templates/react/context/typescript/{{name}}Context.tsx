import React, { createContext, useState } from "react";

// Types
export interface I{{nameProperCase}}ContextState {
  value: string;
}

export interface I{{nameProperCase}}ContextActions {
  setValue: (val: string) => void;
}

export interface I{{nameProperCase}}Context {
  actions: I{{nameProperCase}}ContextActions;
  state: I{{nameProperCase}}ContextState;
}

// Init
export const {{name}}ContextEmptyState: I{{nameProperCase}}ContextState = {
  value: "",
};

const emptyAction = () => {};
export const {{name}}ContextEmptyActions: I{{nameProperCase}}ContextActions = {
  setValue: () => emptyAction,
};

export const {{nameProperCase}}Context = createContext<I{{nameProperCase}}Context>({
  state: {{name}}ContextEmptyState,
  actions: {{name}}ContextEmptyActions,
});

export const {{nameProperCase}}ContextConsumer = {{nameProperCase}}Context.Consumer;

// Implementation
interface I{{nameProperCase}}ContextProviderParams {
  children: JSX.Element[] | JSX.Element;
  initialState?: I{{nameProperCase}}ContextState;
}
export const {{nameProperCase}}ContextProvider = ({ children, initialState }: I{{nameProperCase}}ContextProviderParams) => {
  const [state, updateState] = useState<I{{nameProperCase}}ContextState>(initialState || {{name}}ContextEmptyState);

  const actions: I{{nameProperCase}}ContextActions = {
    setValue: (val) => {
      updateState({ ...state, value: val });
    },
  };

  return <{{nameProperCase}}Context.Provider value={{{{ actions, state }}}}>{children}</{{nameProperCase}}Context.Provider>;
};

export default { {{nameProperCase}}Context, {{nameProperCase}}ContextProvider };
