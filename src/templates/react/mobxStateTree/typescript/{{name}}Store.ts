import { values } from "mobx";
import { types } from "mobx-state-tree";

export type I{{nameProperCase}}Store = {
  value: string;
  readonly computedValue: readonly any[];
  setValue(newValue: string): void;
}

export const {{name}}Model = types
  .model({
    value: types.optional(types.string, ""),
  })
  .views((self) => ({
    get computedValue() {
      return values(self.value);
    },
  }))
  .actions((self) => ({
    setValue(newValue: string) {
      self.value = newValue;
    },
  }));

export const {{name}}Store: I{{nameProperCase}}Store = {{name}}Model.create();
