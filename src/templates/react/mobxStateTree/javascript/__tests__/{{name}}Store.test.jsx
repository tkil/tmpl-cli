import React from "react";
import { fireEvent, render } from "@testing-library/react";
import { observer } from "mobx-react";

import { {{name}}Store } from "../{{name}}Store";

describe("{{name}}Store", () => {
  describe("setValue()", () => {
    it("should set value to foo", () => {
      // Arrange
      const Component = observer((props) => {
        return (
          <>
            <span data-testid="action" onClick={() => props.{{name}}Store.setValue("foo")}></span>
            <span data-testid="result">{props.{{name}}Store.value}</span>
          </>
        );
      });
      const { getByTestId } = render(<Component {{name}}Store={ {{name}}Store} />);
      // Act
      fireEvent.click(getByTestId("action"));
      // Assert
      const actual = getByTestId("result").textContent;
      expect(actual).toEqual("foo");
    });
  });
});
