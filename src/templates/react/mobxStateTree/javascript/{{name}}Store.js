import { values } from "mobx";
import { types } from "mobx-state-tree";

export const {{name}}Model = types
  .model({
    value: types.optional(types.string, ""),
  })
  .views((self) => ({
    get computedValue() {
      return values(self.value);
    },
  }))
  .actions((self) => ({
    setValue(newValue: string) {
      self.value = newValue;
    },
  }));

export const {{name}}Store = {{name}}Model.create();
