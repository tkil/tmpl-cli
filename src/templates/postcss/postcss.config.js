const autoprefixer = require(`autoprefixer`);
const postcssPresetEnv = require(`postcss-preset-env`);
const postcssCustomProperties = require("postcss-custom-properties");
const postCssImport = require("postcss-import");
const cssNano = require("cssnano");

module.exports = () => ({
  plugins: [
    autoprefixer(),
    postCssImport(),
    postcssPresetEnv({
      stage: 3
    }),
    postcssCustomProperties({
      preserve: false
    }),
    cssNano({
      preset: [
        "default",
        {
          discardComments: { removeAll: true }
        }
      ]
    })
  ]
});
