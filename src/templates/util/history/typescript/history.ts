export const setPageInCurrentHistoryState = (page: string) => {
  window.history.replaceState({ page }, page);
};

export const pushPageToBrowserHistory = (page: string) => {
  window.history.pushState({ page }, page);
};

export const onHistoryPopGetPage = (cb: (page: string) => void) => {
  window.onpopstate = (event: any) => {
    cb(event.state);
  };
};
