export const setPageInCurrentHistoryState = page => {
  window.history.replaceState({ page }, page);
};

export const pushPageToBrowserHistory = page => {
  window.history.pushState({ page }, page);
};

export const onHistoryPopGetPage = cb => {
  window.onpopstate = event => {
    cb(event.state);
  };
};
