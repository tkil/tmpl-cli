export const delay = milliseconds => new Promise(res => setTimeout(res, milliseconds));
