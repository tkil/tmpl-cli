export interface IListener {
  event: string;
  fn: () => void;
  target: any;
}
export const registeredEventListeners = new Map();

export const registerEventListener = (name: string, listener: IListener) => {
  registeredEventListeners.set(name, listener);
  const l = registeredEventListeners.get(name);
  l.target.addEventListener(l.event, l.fn);
};

export const unregisterEventListener = (name: string) => {
  const l = registeredEventListeners.get(name);
  l.target.removeEventListener(l.event, l.fn);
  registeredEventListeners.delete(name);
};

export const unregisterAllEventListeners = () => {
  for (const key of registeredEventListeners.keys()) {
    unregisterEventListener(key);
  }
};
