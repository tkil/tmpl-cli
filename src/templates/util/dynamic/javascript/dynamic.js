const loadedScripts = new Set();
const loadedStyles = new Set();

/**
 * @param {string} url
 * @return {void}
 */
export const loadExternalScript = url => {
  if (!loadedScripts.has(url)) {
    const script = document.createElement("script");
    script.src = url;
    script.type = "text/javascript";
    document.body.appendChild(script);
    loadedScripts.add(url);
  }
};

/**
 * @param {string} url
 * @return {void}
 */
export const loadExternalStylesheet = url => {
  if (!loadedStyles.has(url)) {
    const link = document.createElement("link");
    link.href = url;
    link.rel = "stylesheet";
    link.type = "text/css";
    document.head.appendChild(link);
    loadedStyles.add(url);
  }
};
