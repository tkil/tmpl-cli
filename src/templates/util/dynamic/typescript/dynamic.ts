const loadedScripts = new Set<string>();
const loadedStyles = new Set<string>();

export const loadExternalScript = (url: string) => {
  if (!loadedScripts.has(url)) {
    const script = document.createElement("script");
    script.src = url;
    script.type = "text/javascript";
    document.body.appendChild(script);
    loadedScripts.add(url);
  }
};

export const loadExternalStylesheet = (url: string) => {
  if (!loadedStyles.has(url)) {
    const link = document.createElement("link");
    link.href = url;
    link.rel = "stylesheet";
    link.type = "text/css";
    document.head.appendChild(link);
    loadedStyles.add(url);
  }
};
