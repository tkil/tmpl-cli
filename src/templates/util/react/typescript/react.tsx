import React from "react";
import { hydrate, render } from "react-dom";

export const initReact = (rootId: string, method: "render" | "hydrate", jsx: JSX.Element) => {
  const rootElement = document.getElementById(rootId);
  if (rootElement) {
    if (method === "render") {
      render(jsx, rootElement);
    } else {
      hydrate(jsx, rootElement);
    }
  } else {
    console.error(`Could not find element with ID: ${rootId}`);
  }
};

// window.addEventListener("DOMContentLoaded", () => initReact("root", "render", <div>React Working</div>));
