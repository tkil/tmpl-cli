import { retry } from "../retry";

const failNTimesSpyFnFactory = (failTimes = 0) => {
  let calledTimes = 0;
  return jest.fn(() => {
    if (calledTimes < failTimes) {
      calledTimes = calledTimes + 1;
      throw new Error(`Function fail: ${calledTimes}`);
    }
  });
};

const defaultRetryOptionsForTests = {
  exponentialRetryFactor: 1,
  maxRetries: 3,
  retryInterval: 100,
  retryTimeLimit: 5000,
  isExceptionThrownOnFailure: true,
  failureCallback: () => new Promise((res: any) => res()),
  retryCallback: () => new Promise((res: any) => res()),
  successCallback: () => new Promise((res: any) => res()),
};

describe("Retry", () => {
  describe("retry()", () => {
    let successSpyFn: jest.Mock<any, any>;
    let failSpyFn: jest.Mock<any, any>;

    beforeEach(() => {
      successSpyFn = jest.fn();
      failSpyFn = jest.fn(() => {
        throw new Error(`Function always fails`);
      });
    });

    it("should return the value of the tried function", async (done) => {
      // Arrange
      const fn = () => "The answer is 42...";
      // Act
      const answer = await retry(fn, defaultRetryOptionsForTests);
      // Assert
      expect(answer).toEqual("The answer is 42...");
      done();
    });

    it("should invoke the attempt function once", async (done) => {
      // Arrange
      // Act
      await retry(successSpyFn, defaultRetryOptionsForTests);
      // Assert
      expect(successSpyFn).toBeCalledTimes(1);
      done();
    });

    it("should invoke the attempt function a max of 5 times", async (done) => {
      // Arrange
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          maxRetries: 4,
          retryInterval: 10,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        expect(failSpyFn).toBeCalledTimes(5);
        done();
      }
    });

    it("should invoke the attempt function a max of 7 times", async (done) => {
      // Arrange
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          maxRetries: 6,
          retryInterval: 10,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        expect(failSpyFn).toBeCalledTimes(7);
        done();
      }
    });

    it("should invoke the attempt function a second time after a failure", async (done) => {
      // Arrange
      const failOneTimesSpyFn = failNTimesSpyFnFactory(1);
      // Act
      await retry(failOneTimesSpyFn, defaultRetryOptionsForTests);
      // Assert
      expect(failOneTimesSpyFn).toBeCalledTimes(2);
      done();
    });

    it("should invoke the attempt function a third time after two failures", async (done) => {
      // Arrange
      const failTwoTimesSpyFn = failNTimesSpyFnFactory(2);
      // Act
      await retry(failTwoTimesSpyFn, defaultRetryOptionsForTests);
      // Assert
      expect(failTwoTimesSpyFn).toBeCalledTimes(3);
      done();
    });

    it("should invoke the successCallback", async (done) => {
      // Arrange
      const successCallbackSpy = jest.fn();
      // Act
      await retry(successSpyFn, {
        ...defaultRetryOptionsForTests,
        successCallback: successCallbackSpy,
      });
      // Assert
      expect(successCallbackSpy).toBeCalledTimes(1);
      done();
    });

    it("should invoke the failureCallback", async (done) => {
      // Arrange
      const failureCallbackSpy = jest.fn();
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          failureCallback: failureCallbackSpy,
          maxRetries: 1,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        expect(failureCallbackSpy).toBeCalledTimes(1);
        done();
      }
    });

    it("should invoke the retryCallback twice", async (done) => {
      // Arrange
      const retryCallbackSpy = jest.fn();
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          retryCallback: retryCallbackSpy,
          maxRetries: 2,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        expect(retryCallbackSpy).toBeCalledTimes(2);
        done();
      }
    });

    it("should have a retryTimeLimit of 2 seconds", async (done) => {
      // Arrange
      const retryCallbackSpy = jest.fn();
      const expectedEpochTimeReached = new Date().valueOf() + 2000;
      const expectedEpochTimeNotReached = new Date().valueOf() + 2100;
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          retryCallback: retryCallbackSpy,
          maxRetries: Number.MAX_VALUE,
          exponentialRetryFactor: 1,
          retryTimeLimit: 2000,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        const endEpochTime = new Date().valueOf();
        expect(endEpochTime).toBeGreaterThan(expectedEpochTimeReached);
        expect(endEpochTime).toBeLessThan(expectedEpochTimeNotReached);
        done();
      }
    });

    it("should have a retryInterval of 50ms", async (done) => {
      // Arrange
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          maxRetries: Number.MAX_VALUE,
          exponentialRetryFactor: 1,
          retryInterval: 50,
          retryTimeLimit: 990,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        expect(failSpyFn).toBeCalledTimes(20);
        done();
      }
    });

    it("should have a exponentialRetryFactor of 1.25", async (done) => {
      // Arrange
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          maxRetries: Number.MAX_VALUE,
          exponentialRetryFactor: 1.25,
          retryInterval: 50,
          retryTimeLimit: 990,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        expect(failSpyFn).toBeCalledTimes(9);
        done();
      }
    });

    it("should have a exponentialRetryFactor of 2", async (done) => {
      // Arrange
      // Act
      try {
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          maxRetries: Number.MAX_VALUE,
          exponentialRetryFactor: 2,
          retryInterval: 50,
          retryTimeLimit: 990,
        });
        fail("Test failed, should assert in test catch");
        done();
      } catch (error) {
        // Assert
        expect(failSpyFn).toBeCalledTimes(5);
        done();
      }
    });

    it("should throw an exception on failure", async (done) => {
      // Arrange
      const fn = async () => {
        // Act
        await retry(failSpyFn, {
          ...defaultRetryOptionsForTests,
          isExceptionThrownOnFailure: true,
          maxRetries: 1,
        });
      };
      // Assert
      await expect(fn).rejects.toEqual(new Error("Function always fails"));
      done();
    });

    it("should NOT throw an exception on failure", async (done) => {
      // Arrange
      const failCallbackSpy = jest.fn();
      // Act
      const result = await retry(failSpyFn, {
        ...defaultRetryOptionsForTests,
        failureCallback: failCallbackSpy,
        isExceptionThrownOnFailure: false,
        maxRetries: 1,
      });
      // Assert
      expect(result).toBeUndefined();
      expect(failCallbackSpy).toBeCalledTimes(1);
      done();
    });
  });
});
