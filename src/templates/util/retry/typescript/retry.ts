interface IRetryOptions {
  exponentialRetryFactor?: number;
  maxRetries?: number;
  retryInterval?: number;
  retryTimeLimit?: number;
  isExceptionThrownOnFailure?: boolean;
  failureCallback?: () => void;
  retryCallback?: () => void;
  successCallback?: () => void;
}

interface IRetryOptionsMandatory {
  exponentialRetryFactor: number;
  maxRetries: number;
  retryInterval: number;
  retryTimeLimit: number;
  isExceptionThrownOnFailure: boolean;
  failureCallback?: () => void;
  retryCallback?: () => void;
  successCallback?: () => void;
}

const defaultRetryOptions: IRetryOptionsMandatory = {
  exponentialRetryFactor: 2,
  maxRetries: 3,
  retryInterval: 1000,
  retryTimeLimit: 30 * 1000,
  isExceptionThrownOnFailure: true,
};

const delay = (milliseconds: number) => new Promise((res) => setTimeout(res, milliseconds));

const epochNow = () => new Date().valueOf();

export const retry = async (
  attemptCallback: () => any | Promise<any>,
  options: IRetryOptions = defaultRetryOptions
) => {
  const opt: IRetryOptionsMandatory = { ...defaultRetryOptions, ...options };
  let isSuccessful = false;
  let attempts = 0;
  let giveUpEpoch = epochNow() + opt.retryTimeLimit;

  const attemptWrapper = async (cb: () => any | Promise<any>) => {
    try {
      const result = await cb();
      if (opt.successCallback) {
        opt.successCallback();
      }
      isSuccessful = true;
      return result;
    } catch (error) {
      if (attempts < opt.maxRetries && epochNow() < giveUpEpoch) {
        if (opt.retryCallback) {
          opt.retryCallback();
        }
        attempts = attempts + 1;
      } else {
        throw error;
      }
    }
  };

  try {
    while (attempts <= opt.maxRetries && !isSuccessful) {
      const result = await attemptWrapper(attemptCallback);
      if (isSuccessful) {
        return result;
      }
      await delay(opt.retryInterval * Math.pow(opt.exponentialRetryFactor, attempts));
    }
  } catch (error) {
    if (opt.failureCallback) {
      opt.failureCallback();
    }
    if (opt.isExceptionThrownOnFailure) {
      throw error;
    }
  }
};
