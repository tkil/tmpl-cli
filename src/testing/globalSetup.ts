import { createTestingDir } from "./io";

const globalSetup = () => {
  process.env.IS_AUTOMATED = "true";
  createTestingDir(".");
};

export default globalSetup;
