import { mkdirSync } from "fs";
import * as path from "path";
import { sync as rimraf } from "rimraf";

export const testingDirName = ".testing";

export const getTestingDirPath = (dirName: string) => path.resolve(__dirname, "..", "..", testingDirName, dirName);

export const createTestingDir = (dirName: string) => {
  try {
    removeTestingDir(getTestingDirPath(dirName));
    mkdirSync(getTestingDirPath(dirName));
  } catch (err) {
    console.warn(err);
  }
};

export const removeTestingDir = (dirName: string) => {
  try {
    rimraf(getTestingDirPath(dirName));
  } catch (err) {}
};

export const testSetup = async (dirName: string) => {
  await createTestingDir(dirName);
  process.chdir(getTestingDirPath(dirName));
};

export const tearDown = async (dirName: string) => {
  await removeTestingDir(dirName);
};
