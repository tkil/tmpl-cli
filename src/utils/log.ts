import * as chalk from "chalk";
import * as _log from "woodchuck";

_log.config.setOptions({
  logWarnFn: payload => _log.warn({ ...payload, message: chalk.yellow(payload.message) }),
  logErrorFn: payload => _log.error({ ...payload, message: chalk.yellow(payload.message) })
});

export const log = _log;

export default _log;
