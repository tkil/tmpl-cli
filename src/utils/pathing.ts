export const resolvePath = (defaultPath: string, nameArg: string) => {
  const pathArr = nameArg.split("/");
  if (pathArr.length > 1) {
    return {
      dirPath: "src/" + pathArr.slice(0, pathArr.length - 1).join("/"),
      name: pathArr[pathArr.length - 1]
    };
  }
  return {
    dirPath: "src/" + defaultPath,
    name: nameArg
  };
};
