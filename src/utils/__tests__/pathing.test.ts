import { resolvePath } from "../pathing";

describe("Utils: Pathing", () => {
  describe("resolvePath()", () => {
    it("should return the default path", () => {
      // Act
      const actual = resolvePath("module", "foo");
      // Assert
      const expected = {
        name: "foo",
        dirPath: "src/module"
      };
      expect(actual).toEqual(expected);
    });
    it("should return a custom path", () => {
      // Act
      const actual = resolvePath("module", "utils/shared/foo");
      // Assert
      const expected = {
        name: "foo",
        dirPath: "src/utils/shared"
      };
      expect(actual).toEqual(expected);
    });
  });
});
