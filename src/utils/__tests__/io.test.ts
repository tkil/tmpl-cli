// Project
import { injectStringBelowCommentTagOfFile } from "../io";

describe("Utils: IO", () => {
  describe("injectStringBelowCommentTagOfFile()", () => {
    it.skip("should inject string below comment tag", () => {
      // Arrange
      const data = `// Tag
      
      Blah Blah
      Stuff
      Uhhh...
      `;
      // Act
      const actual = false;
      // Assert
      const expected = true;
      expect(actual).toEqual(expected);
    });
  });
});
