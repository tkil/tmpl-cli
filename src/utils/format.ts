import { IDocCommand } from "types";

export const docCommandsToExamples = (docCommands: IDocCommand[]) =>
  docCommands.map(d => `${d.summaryShort ? d.summaryShort + "\n" : ""}$ ${d.command}\n`);

export const toKebabCase = (str: string): string =>
  (str.match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g) || [])
    .map(c => c.toLowerCase())
    .join("-");

export const toProperCase = (str: string): string => str.slice(0, 1).toLocaleUpperCase() + str.slice(1, str.length);

export const getExtension = (filePath: string) => {
  const parts = filePath.split(".");
  return "." + parts[parts.length - 1];
};
