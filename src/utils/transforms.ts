import { IObjAny, IObjString } from "types";

export const arrayToLines = (str: Array<string | undefined>, delimiter = "\n") =>
  str.filter(i => i !== undefined).join(delimiter);

export const arrayToInStringArray = (str: Array<string | undefined>, delimiter = ", ") =>
  `[${str
    .filter(i => Boolean(i))
    .map(i => `${i}`)
    .join(delimiter)}]`;

export const mapObjAnyToObjString = (items: IObjAny): IObjString =>
  Object.fromEntries(Object.keys(items).map(key => [key, String(items[key])]));
