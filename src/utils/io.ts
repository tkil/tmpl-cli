import { exec } from "child_process";
import { promisify } from "util";
import * as path from "path";
import { promises as fsAsync } from "fs";
import { copyFileSync, mkdirSync, readFileSync, writeFileSync } from "fs";
import * as formatJson from "format-json";

import { log } from "@utils/log";

import { getExtension } from "@utils/format";

export const generatorsDir = path.resolve(__dirname, "..", "generators");

export const templatesDir = path.resolve(__dirname, "..", "templates");

export const copyDrill = (from: string, to: string) => {
  const mkdirQuite = (path: string) => {
    try {
      mkdirSync(path);
    } catch {}
  };
  let pathSteps = ".";
  const steps = to.split("/");
  for (let i = 1; i < steps.length - 1; i++) {
    pathSteps = `${pathSteps}/${steps[i]}`;
    mkdirQuite(pathSteps);
  }
  copyFileSync(from, to);
};

export const delay = (timeInMs = 100) => new Promise(resolve => setTimeout(resolve, timeInMs));

export const exists = async (filePath: string): Promise<boolean> => {
  try {
    await fsAsync.stat(filePath);
    return true;
  } catch (err) {
    return false;
  }
};

export const prettyFile = async (path: string) => {
  const extension = getExtension(path);
  try {
    switch (extension) {
      case ".js":
      case ".jsx":
        await promisify(exec)(`prettier --write ${path} --parser babel`);
        return;
      case ".ts":
      case ".tsx":
        await promisify(exec)(`prettier --write ${path} --parser typescript`);
        return;
      // case ".html":
      // case ".vue":
      //   await promisify(exec)(`prettier --write ${path} --parser html`);
      //   return;
      case ".json":
        let jsonData = await fsAsync.readFile(path, "utf8");
        jsonData = formatJson.plain(JSON.parse(jsonData));
        await fsAsync.writeFile(path, jsonData, "utf8");
        return;
    }
  } catch (err) {}

  log.warn(`No file formatting configured for ${extension} file types.`);
};

export const injectStringBelowCommentTagOfFile = (
  filePath: string,
  commentTag: string,
  str: string,
  cb: (error: Error | null, success: any) => void
) => {
  const data = readFileSync(filePath, "utf8");
  const lines = data.split("\n");
  for (let i = 0; i < lines.length; i++) {
    if (new RegExp(`\/\/\ *${commentTag}`).test(lines[i])) {
      const updatedLines = [...lines.slice(0, i), str, ...lines.slice(i, lines.length)];
      writeFileSync(filePath, updatedLines, "utf8");
      cb(null, null);
      return;
    }
  }
};

export const injectStringAtTopOfFile = () => {};
