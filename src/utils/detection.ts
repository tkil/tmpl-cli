import { existsSync } from "fs";

export const isTypescriptProject = (
  flags: { typescript?: boolean; javascript?: boolean } = { typescript: false, javascript: false }
) => !flags.javascript && (flags.typescript || existsSync("./tsconfig.json"));
