import { flags } from "@oclif/command";

export const installFlag = {
  install: flags.boolean({ char: "i", description: "Active install instead of dep add" })
};

export const javascriptFlags = {
  javascript: flags.boolean({ char: "j", description: "force javascript templates" }),
  typescript: flags.boolean({ char: "t", description: "force typescript templates" })
};

export const standardFlags = {
  help: flags.help({ char: "h" }),
  force: flags.boolean({ char: "f", description: "force install and overwrite" }),
  "dry-run": flags.boolean({ char: "d", description: "preview install with no changes" })
};

export const hintFlag = {
  help: flags.help({ char: "H", description: "include hints" })
};
