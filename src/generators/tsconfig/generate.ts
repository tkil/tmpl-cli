// Project
import { tsConfigJsxReact, tsConfigFilesVueShim } from "@templates/tsConfig/fragments";
import { IGeneratorResult } from "types";

interface ITsConfigOptions {
  isReact?: boolean;
  isVue?: boolean;
}

export const generateTsConfig = (options: ITsConfigOptions, path: string = "."): IGeneratorResult => {
  const vars = {
    tsConfigJsxReact: options.isReact ? tsConfigJsxReact : "",
    tsConfigFilesVueShim: options.isVue ? tsConfigFilesVueShim : ""
  };

  const files = [["/tsConfig/core", path, vars]];

  return {
    files,
    devPackages: [],
    prodPackages: []
  };
};
