import { PACKAGE } from "@packages";
import {
  jestTypescriptGlobals,
  jestTransformJavascript,
  jestTransformTypescript,
  jestTransformVue
} from "@templates/jest/fragments";
import { IGeneratorResult } from "types";

interface IJestOptions {
  isTypescript?: boolean;
  isVue?: boolean;
}
export const generateJest = (options: IJestOptions, path: string = ""): IGeneratorResult => {
  // Files
  const stampVars = {
    valSrcIndex: options.isTypescript ? "index.ts" : "index.js",
    jestTypescriptGlobals: options.isTypescript ? jestTypescriptGlobals : "",
    jestTransformJavascript: !options.isTypescript ? jestTransformJavascript : "",
    jestTransformTypescript: options.isTypescript ? jestTransformTypescript : "",
    jestTransformVue: options.isVue ? jestTransformVue : ""
  };
  const files = [["/jest/core", path, stampVars]];

  // Packages
  let devPackages = [PACKAGE.jest];
  const prodPackages: string[] = [];
  if (!options.isTypescript) {
    devPackages = [...devPackages, PACKAGE.babelJest, PACKAGE.babelCore];
  }
  if (options.isTypescript) {
    devPackages = [...devPackages, PACKAGE.typescript, PACKAGE.tsJest, PACKAGE.typesJest];
  }
  if (options.isVue) {
    devPackages = [...devPackages, PACKAGE.vueJest, PACKAGE.vueTestUtils, PACKAGE.babelCoreBridge];
  }

  return {
    files,
    devPackages,
    prodPackages
  };
};
