import { toProperCase, toKebabCase } from "@utils/format";
import { mapObjAnyToObjString } from "@utils/transforms";
import { IGenerateReactFcOptions, IGenerateReactFcOptionsDerived } from "./generateReactFc";
import {
  genComponentConnected,
  genComponentDefinition,
  genDefaultProps,
  genImportContext,
  genImportLibrary,
  genImportMobxStore,
  genImportQuery,
  genInterfaceProps
} from "./parts";

const deriveOptions = (opt: IGenerateReactFcOptions): IGenerateReactFcOptionsDerived => ({
  name: opt.name,
  nameKebabCase: toKebabCase(opt.name),
  nameProperCase: toProperCase(opt.name),
  contextName: opt.contextName,
  contextNameProperCase: toProperCase(opt.contextName),
  contextNameNoSuffix: opt.contextName.replace("Context", ""),
  mobxName: opt.mobxName,
  mobxNameProperCase: toProperCase(opt.mobxName),
  fileExtension: opt.isTypescript ? "tsx" : "jsx",
  isQuery: opt.isQuery,
  isTypescript: opt.isTypescript
});

export const generateReactFcVars = async (opt: IGenerateReactFcOptions) => {
  const derivedOptions = deriveOptions(opt);
  const stampVars = {
    ...mapObjAnyToObjString(derivedOptions),
    genComponentConnected: genComponentConnected(derivedOptions),
    genComponentDefinition: genComponentDefinition(derivedOptions),
    genDefaultProps: genDefaultProps(derivedOptions),
    genImportContext: genImportContext(derivedOptions),
    genImportLibrary: genImportLibrary(derivedOptions),
    genImportMobxStore: genImportMobxStore(derivedOptions),
    genImportQuery: genImportQuery(derivedOptions),
    genInterfaceProps: genInterfaceProps(derivedOptions)
  };
  return stampVars;
};
