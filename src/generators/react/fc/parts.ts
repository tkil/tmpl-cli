import { IGenerateReactFcOptionsDerived } from "./generateReactFc";
import { arrayToLines } from "../../../utils/transforms";

export const genImportLibrary = (opt: IGenerateReactFcOptionsDerived) =>
  arrayToLines([
    `import React ${opt.contextName ? ", { useContext } " : ""}from "react";`,
    opt.mobxName ? 'import { observer } from "mobx-react"' : undefined
  ]);

export const genImportContext = (opt: IGenerateReactFcOptionsDerived) =>
  opt.contextName
    ? arrayToLines([
        "",
        "import {",
        opt.isTypescript ? `  I${opt.contextNameProperCase}Actions,` : undefined,
        opt.isTypescript ? `  I${opt.contextNameProperCase}State,` : undefined,
        `  ${opt.contextNameProperCase},`,
        `  ${opt.contextName}EmptyState,`,
        `  ${opt.contextName}EmptyActions,`,
        `} from "@stores/${opt.contextName}";`
      ])
    : "";

export const genImportMobxStore = (opt: IGenerateReactFcOptionsDerived) =>
  opt.mobxName
    ? arrayToLines([
        "",
        opt.isTypescript
          ? `import { ${opt.mobxName}, I${opt.mobxNameProperCase} } from '@stores/${opt.mobxName}';`
          : `import { ${opt.mobxName} } from '@stores/${opt.mobxName}';`
      ])
    : "";

export const genImportQuery = (opt: IGenerateReactFcOptionsDerived) => {
  return opt.isQuery ? arrayToLines(["", `import { ${opt.name}Query } from "./${opt.name}.query";`]) : "";
};

export const genInterfaceProps = (opt: IGenerateReactFcOptionsDerived) =>
  opt.isTypescript
    ? arrayToLines([
        "",
        `export interface I${opt.nameProperCase}Props {`,
        "",
        opt.mobxName ? `  ${opt.mobxName}?: I${opt.mobxNameProperCase};` : undefined,
        opt.contextName ? `  ${opt.contextNameNoSuffix}Data?: I${opt.contextNameProperCase}State;` : undefined,
        opt.contextName ? `  ${opt.contextNameNoSuffix}Actions?: I${opt.contextNameProperCase}Actions;` : undefined,
        "}",
        ""
      ])
    : "";

export const genDefaultProps = (opt: IGenerateReactFcOptionsDerived) =>
  arrayToLines([
    `const defaultProps${opt.isTypescript ? ": IProps" : ""} = {`,
    '  propData: "",',
    opt.contextName ? `  ${opt.contextNameNoSuffix}Actions: ${opt.contextName}EmptyActions,` : undefined,
    opt.contextName ? `  ${opt.contextNameNoSuffix}Data: ${opt.contextName}EmptyState,` : undefined,
    "}"
  ]);

export const genComponentDefinition = (opt: IGenerateReactFcOptionsDerived) =>
  arrayToLines([
    `export const ${opt.nameProperCase}${opt.isTypescript ? `: React.FC<I${opt.nameProperCase}Props>` : ""} = (props${
      opt.isTypescript ? `: I${opt.nameProperCase}Props` : ""
    }) => {`,
    opt.isQuery ? "  // Gatsby Data" : undefined,
    opt.isQuery ? `  const query = ${opt.name}Query();` : undefined,
    opt.isQuery ? "" : undefined,
    "  // State",
    "",
    "  // Computed",
    "",
    "  // Effects",
    "",
    "  // Event Handlers",
    "",
    "  // Template",
    "  return (",
    `    <div className="${opt.nameKebabCase}" role="none">`,
    `      -- ${opt.nameProperCase} Component --`,
    `    </div>`,
    "  );",
    `};`
  ]);

export const genComponentConnected = (opt: IGenerateReactFcOptionsDerived) => {
  switch (true) {
    case Boolean(opt.contextName):
      return arrayToLines([
        "",
        `export const ${opt.name}Connected = () => {`,
        `  const ${opt.contextName} = useContext(${opt.contextNameProperCase});`,
        `  return <${opt.nameProperCase} ${opt.contextNameNoSuffix}Actions={ ${opt.contextName}.actions } ${opt.contextNameNoSuffix}Data={ ${opt.contextName}.state }></${opt.nameProperCase}>;`,
        "};",
        ""
      ]);
    case Boolean(opt.mobxName):
      return arrayToLines([
        "",
        `export const ${opt.nameProperCase}Connected = () => <${opt.nameProperCase} ${opt.mobxName}={${opt.mobxName}}/>;`,
        ""
      ]);
  }
};
