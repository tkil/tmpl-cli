export interface IGenerateReactFcOptions {
  name: string;
  contextName: string;
  mobxName: string;
  isTypescript: boolean;
  isQuery: boolean;
}

export interface IGenerateReactFcOptionsDerived {
  name: string;
  nameKebabCase: string;
  nameProperCase: string;
  contextName: string;
  contextNameProperCase: string;
  contextNameNoSuffix: string;
  mobxName: string;
  mobxNameProperCase: string;
  fileExtension: string;
  isQuery: boolean;
  isTypescript: boolean;
}
