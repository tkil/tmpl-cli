import { PACKAGE } from "@packages";
import { babelPresetReact } from "@templates/babel/fragments";
import { IGeneratorResult } from "types";

interface IWebpackOptions {
  isReact?: boolean;
}

export const generateBabel = (options: IWebpackOptions, path: string = "."): IGeneratorResult => {
  // Files
  const stampVars = {
    babelPresetReact: options.isReact ? babelPresetReact : ""
  };
  const files = [["/babel/core", path, stampVars]];

  // Packages
  let devPackages = [PACKAGE.babelCore, PACKAGE.babelCoreBridge, PACKAGE.babelPresetEnv];
  const prodPackages: string[] = [];
  if (options.isReact) {
    devPackages = [...devPackages, PACKAGE.babelPresetReact];
  }

  return {
    files,
    devPackages,
    prodPackages
  };
};
