import { IWebpackOptions } from "../webpackTypes";

export const getTarget = (opt: IWebpackOptions) => (opt.isNode ? "node" : `["web", "es5"]`);
