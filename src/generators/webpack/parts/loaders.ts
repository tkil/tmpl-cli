import { IWebpackOptions } from "../webpackTypes";

export const getLoaders = (opt: IWebpackOptions): string =>
  [
    opt.isHbs
      ? `{
        test: /\.hbs$/,
        loader: "handlebars-loader",
        query: {
          partialDirs: [path.join(srcDir, "partials")],
        }
      }`
      : undefined,
    opt.isJavascript
      ? `{
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        exclude: /node_modules/,
      }`
      : undefined,
    opt.isTypescript
      ? `{
        test: /\.(ts|tsx)$/,
        use: "ts-loader",
        include: dir,
        exclude: [nodeModulesDir, /.test.tsx?$/]
      }`
      : undefined,
    opt.isVue
      ? `{
        test: /\\.vue$/,
        loader: ["vue-loader"],
        exclude: /node_modules/,
      }`
      : undefined,
    opt.isWeb
      ? `{
        test: /\.(sa|sc|c)ss$/,
        include: dir,
        enforce: 'pre',
        use: [
          ${[
            `MiniCssExtractPlugin.loader`,
            `{
            loader: 'css-loader',
            options: { importLoaders: 1, sourceMap: !isProduction, url: false }
          }`,
            opt.isVue ? "vue-style-loader" : "",
            opt.isSass
              ? `{
              loader: 'sass-loader',
              options: { sourceMap: true }
          }`
              : undefined,
            `{
            loader: 'postcss-loader',
          }`
          ]
            .filter(i => Boolean(i))
            .join(",\n")}
        ]
      }`
      : undefined,
    opt.isWeb
      ? `{
        test: /\.(png|jp(e*)g|svg|ico)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 512,
              name: 'images/[hash]-[name].[ext]'
            }
          }
        ],
        exclude: nodeModulesDir
      }`
      : undefined
  ]
    .filter(i => Boolean(i))
    .join(",\n");
