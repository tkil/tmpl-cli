import { IWebpackOptions } from "../webpackTypes";
import { arrayToInStringArray } from "../../../utils/transforms";

export const getResolve = (opt: IWebpackOptions) => `
  {
    alias: ${arrayToInStringArray([opt.isVue ? '"alias: vue/dist/vue.esm.js,"' : undefined])},
    extensions: ${arrayToInStringArray([
      '".js"',
      opt.isReact ? '".jsx"' : undefined,
      opt.isTypescript ? '".ts"' : undefined,
      opt.isReact && opt.isTypescript ? '".tsx"' : undefined,
      opt.isVue ? '".vue"' : undefined,
      '".json"'
    ])},
    mainFields: ${arrayToInStringArray([opt.isWeb ? '"browser"' : undefined, '"main"', '"module"'])},
    plugins: ${arrayToInStringArray([
      opt.isTypescript ? 'new TsconfigPathsPlugin({ configFile: path.join(dir, "tsconfig.json") })' : undefined
    ])}
  }
`;
