import { IWebpackOptions } from "../webpackTypes";
import { arrayToLines } from "../../../utils/transforms";

const getDotEnvTemplate = (opt: IWebpackOptions) =>
  arrayToLines([
    "// Env",
    `// const env = require('../../env/config')`,
    "// const envPath = env.envPath",
    "// const envParsed = env.config().parsed`;"
  ]);

const getImportToolsTemplate = (opt: IWebpackOptions) =>
  arrayToLines([
    "// Tools",
    `const path = require("path");`,
    opt.isDotEnv ? 'const DotEnvPlugin = require("dotenv-webpack");' : undefined
  ]);

const getVariablesEnvironment = (opt: IWebpackOptions) =>
  arrayToLines([
    "// Variables - Environment",
    'const webpackMode = process.env.NODE_ENV === "production" ? "production" : "development"',
    'const isProduction = webpackMode === "production"'
  ]);

const getImportPluginsTemplate = (opt: IWebpackOptions) =>
  arrayToLines([
    "// Plugins",
    opt.isCopyPlugin ? "const CopyPlugin = require('copy-webpack-plugin')" : undefined,
    opt.isDotEnv ? 'const DotEnvPlugin = require("dotenv-webpack");' : undefined,
    opt.isWeb ? 'const HtmlWebpackPlugin = require("html-webpack-plugin");' : undefined,
    opt.isWeb ? 'const MiniCssExtractPlugin = require("mini-css-extract-plugin")' : undefined,
    opt.isTypescript ? 'const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin")' : undefined,
    opt.isVue ? 'const VueLoaderPlugin = require("vue-loader/lib/plugin")' : undefined
  ]);

const getVariablesDirectoryTargets = (opt: IWebpackOptions) =>
  arrayToLines([
    "// Variables - Directory Target",
    "const dir = path.resolve(__dirname);",
    'const srcDir = path.join(dir, "src");',
    'const distDir = path.join(dir, "dist");',
    'const nodeModulesDir = path.join(dir, "node_modules");'
  ]);

const getVariablesFileTargets = (opt: IWebpackOptions) =>
  arrayToLines([
    "// Variables - File Targets",
    `const srcIndex = "${opt.isTypescript ? "index.ts" : "index.js"}";`,
    opt.isWeb ? `const srcTemplate = "${opt.isHbs ? "index.hbs" : "index.html"}";` : "",
    opt.isWeb ? 'const distHtml = "index.html"' : ""
  ]);

const getVariablesOther = (opt: IWebpackOptions) =>
  arrayToLines(["// Variables - Other", "const devPort = process.env.DEV_SERVER_PORT || 8080;"]);

export const getPreConfig = (opt: IWebpackOptions) =>
  arrayToLines(
    [
      opt.isDotEnv ? getDotEnvTemplate(opt) : undefined,
      getImportToolsTemplate(opt),
      getImportPluginsTemplate(opt),
      getVariablesDirectoryTargets(opt),
      getVariablesFileTargets(opt),
      getVariablesEnvironment(opt),
      opt.isWeb ? getVariablesOther(opt) : undefined
    ],
    "\n\n"
  );
