import { IWebpackOptions } from "../webpackTypes";

export const getDevServer = (opt: IWebpackOptions) =>
  opt.isWeb
    ? `${"\n"}devServer: {
      compress: true,
      port: devPort,
      open: true,
      openPage: distHtml
    },`
    : "";
