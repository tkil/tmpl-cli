import { IWebpackOptions } from "../webpackTypes";

export const getOutput = (opt: IWebpackOptions) =>
  opt.isWeb
    ? `{
      path: path.resolve(distDir),
      filename: '[name].[contenthash].js'
    }`
    : `{
      path: path.resolve(distDir),
      filename: '[name].js'
    }`;
