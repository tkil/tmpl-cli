import { IWebpackOptions } from "../webpackTypes";

export const getPlugins = (opt: IWebpackOptions) =>
  `[${[
    opt.isDotEnv
      ? `
      new DotEnvPlugin({
        path: envPath, // load this now instead of the ones in '.env'
        systemvars: true // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
      })`
      : undefined,
    opt.isDotEnv
      ? `new webpack.DefinePlugin({
        ENV: JSON.stringify(envParsed)
      })`
      : undefined,
    opt.isWeb
      ? `new HtmlWebpackPlugin({
        ${[
          "filename: distHtml",
          "template: path.join(srcDir, srcTemplate)",
          "// minify: { collapseWhitespace: true }",
          opt.isHbs ? 'isNoIndex: "true"' : undefined,
          opt.isHbs ? 'message: "HBS Working"' : undefined
        ]
          .filter(i => Boolean(i))
          .join(",\n")}
      })`
      : undefined,
    opt.isCopyPlugin
      ? `new CopyPlugin({
        patterns: [
          { from: '', to: distDir },
        ]
      })`
      : undefined,
    opt.isWeb
      ? `new MiniCssExtractPlugin({
        filename: "main.[contenthash].css",
        chunkFilename: "[name].[contenthash].css"
      })`
      : undefined,
    opt.isVue ? "new VueLoaderPlugin()" : undefined
  ]
    .filter(i => Boolean(i))
    .join(",\n")}]`;
