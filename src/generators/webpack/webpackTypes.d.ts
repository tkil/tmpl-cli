export interface IWebpackOptions {
  isDotEnv: boolean;
  isCopyPlugin: boolean;
  isWeb: boolean;
  isNode: boolean;
  isHbs: boolean;
  isCss: boolean;
  isSass: boolean;
  isJavascript: boolean;
  isTypescript: boolean;
  isReact: boolean;
  isVue: boolean;
}
