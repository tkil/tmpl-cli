// Project
import { IWebpackOptions } from "./webpackTypes";
import { getPreConfig } from "./parts/preConfig";
import { getTarget } from "./parts/target";
import { getEntry } from "./parts/entry";
import { getOutput } from "./parts/output";
import { getDevServer } from "./parts/devServer";
import { getDevPackages } from "./packages";
import { getLoaders } from "./parts/loaders";
import { getPlugins } from "./parts/plugins";
import { getResolve } from "./parts/resolve";
import { generateTsConfig } from "../tsconfig/generate";
import { IGeneratorResult } from "types";

interface IWebpackOptionsInput {
  isNode?: boolean;
  isHbs?: boolean;
  isReact?: boolean;
  isSass?: boolean;
  isTypescript?: boolean;
  isVue?: boolean;
}

const normalizeOptions = (optionsInput: IWebpackOptionsInput): IWebpackOptions => ({
  isCopyPlugin: false,
  isDotEnv: false,
  isWeb: !optionsInput.isNode,
  isNode: Boolean(optionsInput.isNode),
  isHbs: Boolean(optionsInput.isHbs),
  isCss: !optionsInput.isSass,
  isSass: Boolean(optionsInput.isSass),
  isJavascript: !optionsInput.isTypescript,
  isTypescript: Boolean(optionsInput.isTypescript),
  isReact: Boolean(optionsInput.isReact),
  isVue: Boolean(optionsInput.isVue)
});

export const generateWebpack = (optionsInput: IWebpackOptionsInput, path: string = ""): IGeneratorResult => {
  // Options
  const opt = normalizeOptions(optionsInput);

  // Files
  const vars = {
    preConfig: getPreConfig(opt),
    target: getTarget(opt),
    entry: getEntry(),
    devServer: getDevServer(opt),
    loaders: getLoaders(opt),
    output: getOutput(opt),
    plugins: getPlugins(opt),
    resolve: getResolve(opt)
  };
  let files = [["/webpack/core", path, vars]];
  if (opt.isWeb) {
    files.push(["/postcss", path]);
  }

  // Packages
  let devPackages = getDevPackages(opt);

  // Generators
  if (opt.isTypescript) {
    const tsConfigGen = generateTsConfig(opt, path);
    files = [...files, ...tsConfigGen.files];
    devPackages = [...devPackages, ...tsConfigGen.devPackages];
  }

  return {
    files,
    devPackages,
    prodPackages: []
  };
};
