import { IWebpackOptions } from "./webpackTypes";

export const getDevPackages = (optionsInput: IWebpackOptions): string[] => {
  let devPackages = ["webpack", "webpack-cli"];
  if (optionsInput.isWeb) {
    devPackages = [
      ...devPackages,
      "webpack-dev-server",
      "html-webpack-plugin",
      "mini-css-extract-plugin",
      "css-loader",
      "autoprefixer",
      "postcss-import",
      "postcss-custom-properties",
      "postcss-loader",
      "postcss-preset-env",
      "cssnano"
    ];
  }

  if (optionsInput.isHbs) {
    devPackages = [...devPackages, "handlebars", "handlebars-loader"];
  }
  if (optionsInput.isJavascript) {
    devPackages = [...devPackages, "babel-loader"];
  }
  if (optionsInput.isTypescript) {
    devPackages = [...devPackages, "typescript", "ts-loader", "tsconfig-paths-webpack-plugin"];
  }
  if (optionsInput.isVue) {
    devPackages = [...devPackages, "vue-loader", "vue-style-loader", "vue-template-compiler"];
  }
  if (optionsInput.isSass) {
    devPackages = [...devPackages, "node-sass", "sass-loader"];
  }
  return devPackages;
};
