import { Command } from "@oclif/command";
import { prompt } from "inquirer";

import { install } from "@engine/installer";
import { generateJest } from "@generators/jest/generate";
import { generateBabel } from "@generators/babel/generate";
import { generateWebpack } from "@generators/webpack/generate";
import { toKebabCase, toProperCase } from "@utils/format";
import { isTypescriptProject } from "@utils/detection";
import { installFlag, javascriptFlags, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

interface IVueOptions {
  isJest: boolean;
  isReact: boolean;
  isVue: boolean;
  isSass: boolean;
  isTypescript: boolean;
}

export default class Vue extends Command {
  static docs: IDocs = {
    description: "Sub cli for Vue project init and generators",
    descriptionShort: "Sub cli for Vue project init and generators",
    commands: [
      {
        summary: "Create a new Vue project.",
        summaryShort: "Create a new Vue project",
        command: "tmpl vue init"
      },
      {
        summary: "Create a vue component.",
        summaryShort: "Create a vue component",
        command: "tmpl vue component [name]"
      }
    ]
  };

  // OCLIF readme gen
  static description = Vue.docs.descriptionShort;
  static examples = docCommandsToExamples(Vue.docs.commands);

  static flags = {
    ...standardFlags,
    ...installFlag,
    ...javascriptFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { args, flags } = this.parse(Vue);

    switch (args.command) {
      case "init":
        await this.initialize(args.arg || ".", flags);
        break;
      case "component":
        await this.componentAction(args.arg, flags);
        break;
      default:
        this.log("No subcommand was provided");
    }
  }

  private async initialize(projectName: string, flags: IFlags) {
    if (projectName) {
      const options = await this.initializePrompt(projectName);
      await this.initializeAction(projectName, options, flags);
      return;
    }
    this.log("Project name was not provided");
  }

  private async initializePrompt(projectName: string) {
    if (process.env.IS_AUTOMATED === "true") {
      return {
        isBabel: false,
        isJest: false,
        isSass: false,
        isReact: false,
        isTypescript: false,
        isVue: true
      };
    }
    const { options } = await prompt({
      type: "checkbox",
      message: "Please select your options",
      name: "options",
      choices: [
        {
          name: "Typescript",
          value: "typescript"
        },
        {
          name: "SCSS",
          value: "sass"
        },
        {
          name: "Jest",
          value: "jest"
        }
      ]
    });
    return {
      isBabel: options.indexOf("typescript") === -1,
      isJest: options.indexOf("jest") > -1,
      isSass: options.indexOf("sass") > -1,
      isReact: false,
      isTypescript: options.indexOf("typescript") > -1,
      isVue: true
    };
  }
  private resolveFlagsToOptions = (flags: IFlags, opt: IVueOptions) => {
    return { ...opt, isTypescript: Boolean(flags.typescript) };
  };

  private async initializeAction(path: string, opt: IVueOptions, flags: IFlags) {
    opt = this.resolveFlagsToOptions(flags, opt);
    // Files
    let files: any[] = [];
    files.push(["/packageJson/web", path]);
    files.push(["/vue/init/core", path, { message: "{{message}}" }]);
    if (opt.isTypescript) {
      files.push(["/vue/init/typescript", path]);
    } else {
      files.push(["/vue/init/javascript", path]);
    }

    // Packages
    let prodPackages = [PACKAGE.vue];
    let devPackages: string[] = [];

    // Generators
    const webpackGen = await generateWebpack(opt, path);
    files = [...files, ...webpackGen.files];
    devPackages = [...devPackages, ...webpackGen.devPackages];
    prodPackages = [...prodPackages, ...webpackGen.prodPackages];
    if (!opt.isTypescript) {
      const babelGen = await generateBabel(opt, path);
      files = [...files, ...babelGen.files];
      devPackages = [...devPackages, ...babelGen.devPackages];
      prodPackages = [...prodPackages, ...babelGen.prodPackages];
    }
    if (opt.isJest) {
      const jestGen = await generateJest(opt, path);
      files = [...files, ...jestGen.files];
      devPackages = [...devPackages, ...jestGen.devPackages];
      prodPackages = [...prodPackages, ...jestGen.prodPackages];
    }
    // Install
    await install(files, devPackages, prodPackages, flags, path);
  }

  private async componentAction(componentName: string, flags: IFlags) {
    // Validation
    if (!componentName) {
      this.log("You must provide the name of the component");
      return;
    }

    // Files
    const vars = {
      componentName,
      componentNameProperCase: toProperCase(componentName),
      componentNameKebabCase: toKebabCase(componentName)
    };
    const files = [
      isTypescriptProject(flags)
        ? ["/vue/component/typescript", "./src/components/", vars]
        : ["/vue/component/javascript", "./src/components/", vars]
    ];

    // Install
    await install(files, [], [], flags);
  }
}
