import { Command, flags } from "@oclif/command";

import { install } from "@engine/installer";
import { eslintTsParser } from "@templates/prettier/fragments";
import { isTypescriptProject } from "@utils/detection";
import { standardFlags, installFlag } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IArgs, IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

interface IFlagsLocal extends IFlags {
  peer: boolean;
}
interface ICommandLocal {
  args: IArgs;
  flags: IFlagsLocal;
}

export default class Prettier extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for prettier",
    commands: [
      {
        summary: "Sub cli for prettier. Installs prettier with eslint bridge to vscode.",
        summaryShort: "Installs prettier with eslint bridge to vscode",
        command: "tmpl prettier"
      },
      {
        summary: "Sub cli for prettier. Installs prettier, eslint bridge, lint-staged and husky.",
        summaryShort: "Installs prettier, eslint bridge, lint-staged and husky",
        command: "tmpl prettier -p"
      }
    ]
  };

  // OCLIF readme gen
  static description = Prettier.docs.descriptionShort;
  static examples = docCommandsToExamples(Prettier.docs.commands);

  static flags = {
    ...standardFlags,
    ...installFlag,
    peer: flags.boolean({ char: "p" })
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    await Prettier.command(this.parse(Prettier));
  }

  static async command({ args, flags }: ICommandLocal) {
    switch (true) {
      case args.command === "peer" || flags.peer:
        await Prettier.actionPeer(flags);
        return;
      default:
        await Prettier.action(flags);
        return;
    }
  }

  private static async action(flags: IFlagsLocal) {
    // Files
    const stampVars = {
      eslintTsParser: isTypescriptProject() ? eslintTsParser : ""
    };
    const files = [["/prettier/core", ".", stampVars]];

    // Packages
    let devPackages = [
      PACKAGE.prettier,
      PACKAGE.eslint,
      PACKAGE.eslintConfigPrettier,
      PACKAGE.eslintPluginPrettier,
      PACKAGE.eslintPluginJest,
      PACKAGE.eslintConfigPrettier,
      PACKAGE.eslintPluginJsxA11y
    ];
    if (isTypescriptProject()) {
      devPackages = [...devPackages, PACKAGE.typescript, PACKAGE.typescriptEslintParser];
    }

    // Install
    await install(files, devPackages, [], flags);
  }

  private static async actionPeer(flags: IFlagsLocal) {
    await Prettier.action(flags);

    // Files
    const files = [
      ["/lintStaged/core", "."],
      ["/prettier/husky", "."]
    ];

    // Packages
    const devPackages = [PACKAGE.husky, PACKAGE.lintStaged];

    // Install
    await install(files, devPackages, [], flags);
  }
}
