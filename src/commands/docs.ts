import { Command } from "@oclif/command";
import { IDocs } from "types";
import { standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import * as express from "express";
import * as open from "open";
import * as path from "path";

export default class Docs extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for tmpl's docs",
    commands: [
      {
        summary:
          "Sub CLI for tmpl's docs. " +
          "Will open a browser page for reviewing tmpl's features and commands.  " +
          "Use ^C to close.  " +
          "Powered by ExpressJs and Docz",
        summaryShort: "Open tmpl's docs in a browser",
        command: "tmpl docs"
      }
    ]
  };

  // OCLIF readme gen
  static description = Docs.docs.descriptionShort;
  static examples = docCommandsToExamples(Docs.docs.commands);
  static flags = {
    ...standardFlags
  };
  async run() {
    await this.action();
  }

  private async action() {
    if (process.env.IS_AUTOMATED === "true") {
      return;
    }
    this.express();
  }

  private async express() {
    const app = express();
    const port = 4110;

    const docDir = path.resolve(__dirname, "..", "docs");
    app.use(express.static(docDir));

    app.listen(port, async () => {
      const url = `http://localhost:${port}/tmpl-cli`;
      this.log(`Opening docs on ${url} ...use ^C to close`);
      await open(url);
    });
  }
}
