import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { installFlag, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

export default class LintStaged extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for lint-staged templates",
    commands: [
      {
        summary: "Sub CLI for lint-staged install. " + "Creates a .lintstagedrc.json file and installs",
        summaryShort: "Creates a .lintstagedrc.jso file and installs",
        command: "tmpl lintstaged"
      }
    ]
  };

  // OCLIF readme gen
  static description = LintStaged.docs.descriptionShort;
  static examples = docCommandsToExamples(LintStaged.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { flags } = this.parse(LintStaged);
    this.action(flags);
  }

  private async action(flags: IFlags) {
    // Files
    const files = [["/lintStaged/core", ""]];

    // Packages
    const devPackages = [PACKAGE.lintStaged];

    // Install
    await install(files, devPackages, [], flags);
  }
}
