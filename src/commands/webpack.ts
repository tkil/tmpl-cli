import { Command } from "@oclif/command";
import { prompt } from "inquirer";

import { install } from "@engine/installer";
import { generateWebpack } from "@generators/webpack/generate";
import { installFlag, standardFlags, javascriptFlags } from "@utils/flags";
import { IFlags } from "types";

export default class Webpack extends Command {
  static description = "Creates webpack.config.js and installs deps";

  static examples = [
    `$ tmpl webpack
`
  ];

  static flags = {
    ...installFlag,
    ...standardFlags,
    ...javascriptFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { flags } = this.parse(Webpack);
    await this.initialize(flags);
  }

  private async initialize(flags: IFlags) {
    const opt = await this.initializePrompt();

    // Generators
    const webpackGen = await generateWebpack(opt);
    const files = webpackGen.files;
    const devPackages = webpackGen.devPackages;
    const prodPackages = webpackGen.prodPackages;

    // Install
    await install(files, devPackages, prodPackages, flags);
  }

  private async initializePrompt() {
    const { options } = await prompt({
      type: "checkbox",
      message: "Please select your options",
      name: "options",
      choices: [
        {
          name: "Typescript",
          value: "typescript"
        },
        {
          name: "SCSS",
          value: "sass"
        }
      ]
    });
    return {
      isBabel: options.indexOf("typescript") === -1,
      isSass: options.indexOf("sass") > -1,
      isTypescript: options.indexOf("typescript") > -1
    };
  }
}
