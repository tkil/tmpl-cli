import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { standardFlags } from "../utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";

interface ICommand {
  flags: IFlags;
}

export default class Python extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Creates a small Python starter project",
    commands: [
      {
        summary: "Creates a small Python start project with a main, test file and makefile",
        summaryShort: "Creates a small Python starter project",
        command: "tmpl python"
      }
    ]
  };

  // OCLIF readme gen
  static description = Python.docs.descriptionShort;
  static examples = docCommandsToExamples(Python.docs.commands);

  static flags = {
    ...standardFlags
  };

  async run() {
    Python.command(this.parse(Python));
  }

  static async command({ flags }: ICommand) {
    await this.action(flags);
  }

  private static async action(flags: IFlags) {
    // Files
    const files = [["/python", "."]];
    // Install
    await install(files, [], [], flags);
  }
}
