import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { generateJest } from "@generators/jest/generate";
import { installFlag, standardFlags, javascriptFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { isTypescriptProject } from "@utils/detection";
import { IDocs, IFlags } from "types";

export default class Jest extends Command {
  static docs: IDocs = {
    description: "Creates a jest.config.js file in the current directory and installs deps",
    descriptionShort: "Sub cli for jest install",
    commands: [
      {
        summary: "Sub CLI for jest install. " + "Creates a jest.config.js and installs.",
        summaryShort: "Creates a jest.config.js and installs",
        command: "tmpl jest"
      },
      {
        summary: "Sub CLI for jest install. " + "Creates a jest.config.js and installs.  " + "Force typescript",
        summaryShort: "Creates a jest.config.js and installs",
        command: "tmpl jest -t"
      }
    ]
  };

  // OCLIF readme gen
  static description = Jest.docs.descriptionShort;
  static examples = docCommandsToExamples(Jest.docs.commands);

  static flags = {
    ...standardFlags,
    ...javascriptFlags,
    ...installFlag
  };
  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { flags } = this.parse(Jest);
    await this.initialize(flags);
  }

  private async initialize(flags: IFlags) {
    // Prompt
    const options = {
      isTypescript: isTypescriptProject(flags)
    };

    // Generators
    const { files, devPackages, prodPackages } = generateJest(options);

    // Install
    await install(files, devPackages, prodPackages, flags);
  }
}
