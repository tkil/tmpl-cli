import { existsSync } from "fs";
import * as path from "path";

import { getTestingDirPath, testSetup, tearDown } from "@testing/io";
import { default as Env } from "@commands/env";

const commandName = Env.name;

describe("Command env", () => {
  beforeAll(async done => {
    await testSetup(commandName);
    await Env.command({ args: {}, flags: { force: false } });
    done();
  });

  afterAll(async done => {
    await tearDown(commandName);
    done();
  });

  it("should create a local .env", async done => {
    const isExisting = existsSync(path.join(getTestingDirPath(commandName), "env/.env.local"));
    expect(isExisting).toEqual(true);
    done();
  });

  it("should create a development .env", async done => {
    const isExisting = existsSync(path.join(getTestingDirPath(commandName), "env/.env.development"));
    expect(isExisting).toEqual(true);
    done();
  });

  it("should create a staging .env", async done => {
    const isExisting = existsSync(path.join(getTestingDirPath(commandName), "env/.env.staging"));
    expect(isExisting).toEqual(true);
    done();
  });

  it("should create a production .env", async done => {
    const isExisting = existsSync(path.join(getTestingDirPath(commandName), "env/.env.production"));
    expect(isExisting).toEqual(true);
    done();
  });

  it("should create a env config", async done => {
    const isExisting = existsSync(path.join(getTestingDirPath(commandName), "env/config.js"));
    expect(isExisting).toEqual(true);
    done();
  });
});
