import { existsSync } from "fs";
import * as path from "path";

import { getTestingDirPath, testSetup, tearDown } from "@testing/io";
import { default as Readme } from "@commands/readme";

const commandName = Readme.name;

describe("Command Readme", () => {
  beforeAll(async done => {
    await testSetup(commandName);
    done();
  });

  afterAll(async done => {
    await tearDown(commandName);
    done();
  });

  it("should create a readme", async done => {
    await Readme.command({ flags: { force: false } });
    const isExisting = existsSync(path.join(getTestingDirPath(commandName), "README.md"));
    expect(isExisting).toEqual(true);
    done();
  });
});
