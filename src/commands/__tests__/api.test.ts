import * as path from "path";

import { default as Api } from "@commands/api";
import { getTestingDirPath, tearDown, testSetup } from "@testing/io";
import { exists } from "@utils/io";

const commandName = Api.name;

describe("Command Api", () => {
  beforeEach(async done => {
    await testSetup(commandName);
    done();
  });

  afterEach(async done => {
    await tearDown(commandName);
    done();
  });

  describe("File Scaffolding", () => {
    it("should create an api file", async done => {
      await Api.command({ args: { name: "foo" }, flags: { force: false } });
      const isExisting = await exists(path.join(getTestingDirPath(commandName), "src/api/foo.js"));
      expect(isExisting).toEqual(true);
      done();
    });

    it("should create an api test file", async done => {
      await Api.command({ args: { name: "foo" }, flags: { force: false } });
      const isExisting = await exists(path.join(getTestingDirPath(commandName), "src/api/__tests__/foo.test.js"));
      expect(isExisting).toEqual(true);
      done();
    });

    it("should create a TS api file", async done => {
      await Api.command({ args: { name: "bar" }, flags: { force: false, typescript: true } });
      const isExisting = await exists(path.join(getTestingDirPath(commandName), "src/api/bar.ts"));
      expect(isExisting).toEqual(true);
      done();
    });
  });
  describe("Pathing", () => {
    it("should dry-run the file at the standard location", async done => {
      // Arrange
      const infoActual = console.info;
      console.info = jest.fn();
      // Act
      await Api.command({ args: { name: "foo" }, flags: { force: false, "dry-run": true } });
      // Assert
      expect((console.info as any).mock.calls[0][0]).toContain("./src/api/foo.js");
      done();
      // Clean up
      console.info = infoActual;
    });
    it("should dry-run the file at a custom location", async done => {
      // Arrange
      const infoActual = console.info;
      console.info = jest.fn();
      // Act
      await Api.command({ args: { name: "custom/shared/foo" }, flags: { force: false, "dry-run": true } });
      // Assert
      expect((console.info as any).mock.calls[0][0]).toContain("./src/custom/shared/foo.js");
      done();
      // Clean up
      console.info = infoActual;
    });
  });
});
