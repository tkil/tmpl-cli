import { default as Node } from "@commands/node";
import { tearDown, testSetup } from "@testing/io";

const commandName = Node.name;

describe("Command Api", () => {
  beforeEach(async done => {
    await testSetup(commandName);
    done();
  });

  afterEach(async done => {
    await tearDown(commandName);
    done();
  });

  describe("Pathing", () => {
    it("should dry-run the file at the standard location", async done => {
      // Arrange
      const infoActual = console.info;
      console.info = jest.fn();
      // Act
      await Node.command({ args: { command: "module", arg: "foo" }, flags: { force: false, "dry-run": true } });
      // Assert
      expect((console.info as any).mock.calls[0][0]).toContain("./src/modules/foo.js");
      done();
      // Clean up
      console.info = infoActual;
    });
    it("should dry-run the file at a custom location", async done => {
      // Arrange
      const infoActual = console.info;
      console.info = jest.fn();
      // Act
      await Node.command({ args: { command: "module", arg: "custom/foo" }, flags: { force: false, "dry-run": true } });
      // Assert
      expect((console.info as any).mock.calls[0][0]).toContain("./src/custom/foo.js");
      done();
      // Clean up
      console.info = infoActual;
    });
  });
});
