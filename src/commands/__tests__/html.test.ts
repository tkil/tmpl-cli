import { default as Html } from "@commands/html";
import { tearDown, testSetup } from "@testing/io";

const commandName = Html.name;

describe("Command Api", () => {
  beforeEach(async done => {
    await testSetup(commandName);
    done();
  });

  afterEach(async done => {
    await tearDown(commandName);
    done();
  });

  describe("Pathing", () => {
    it("should dry-run the file at the standard location", async done => {
      // Arrange
      const infoActual = console.info;
      console.info = jest.fn();
      // Act
      await Html.command({ args: { command: "form", name: "foo" }, flags: { force: false, "dry-run": true } });
      // Assert
      expect((console.info as any).mock.calls[0][0]).toContain("./src/foo.html");
      done();
      // Clean up
      console.info = infoActual;
    });
    it("should dry-run the file at a custom location", async done => {
      // Arrange
      const infoActual = console.info;
      console.info = jest.fn();
      // Act
      await Html.command({ args: { command: "form", name: "custom/foo" }, flags: { force: false, "dry-run": true } });
      // Assert
      expect((console.info as any).mock.calls[0][0]).toContain("./src/custom/foo.html");
      done();
      // Clean up
      console.info = infoActual;
    });
  });
});
