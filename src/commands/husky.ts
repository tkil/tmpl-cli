import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { standardFlags, installFlag } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

export default class Husky extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for Husky install",
    commands: [
      {
        summary: "Sub CLI for Husky install. " + "Creates a .huskyrc and installs",
        summaryShort: "Creates a .huskyrc and installs",
        command: "tmpl husky"
      }
    ]
  };

  // OCLIF readme gen
  static description = Husky.docs.descriptionShort;
  static examples = docCommandsToExamples(Husky.docs.commands);
  static flags = {
    ...standardFlags,
    ...installFlag
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { flags } = this.parse(Husky);
    this.action(flags);
  }

  private async action(flags: IFlags) {
    // Files
    const files = [["/husky/core", "."]];

    // Packages
    const devPackages = [PACKAGE.husky];

    // Install
    await install(files, devPackages, [], flags);
  }
}
