import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { installFlag, standardFlags } from "@utils/flags";
import { isTypescriptProject } from "@utils/detection";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

export default class Schema extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for schema generator",
    commands: [
      {
        summary: "Creates a schema.",
        summaryShort: "Creates a schema",
        command: "tmpl schema [name]"
      }
    ]
  };

  // OCLIF readme gen
  static description = Schema.docs.descriptionShort;
  static examples = docCommandsToExamples(Schema.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags
  };

  static args = [{ name: "name" }];

  async run() {
    const { args, flags } = this.parse(Schema);
    if (args.name) {
      await this.action(args.name, flags);
      return;
    }
    this.log("You must provide the name of the schema");
  }

  private async action(schemaName: string, flags: IFlags) {
    // Files
    const stampVars = {
      schemaName: `${schemaName}Schema`
    };
    const files = [
      isTypescriptProject(flags)
        ? ["/schema/typescript", "./src/data/schemas", stampVars]
        : ["/schema/javascript", "./src/data/schemas", stampVars]
    ];

    // Packages
    const prodPackages = [PACKAGE.validate];

    // Install
    await install(files, [], prodPackages, flags);
  }
}
