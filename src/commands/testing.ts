import { Command } from "@oclif/command";
import { Config } from "@oclif/config";

import { install } from "@engine/installer";
import { installFlag, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { ICommand, IDocs, IFlags } from "types";
import { isTypescriptProject } from "@utils/detection";
import { PACKAGE } from "@packages";
import { prepend } from "@engine/appender";

export default class Testing extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for testing templates",
    commands: [
      {
        summary: "Creates device test helpers.",
        summaryShort: "Creates device test helpers",
        command: "tmpl testing device"
      },
      {
        summary: "Creates dom test helpers.",
        summaryShort: "Creates dom test helpers",
        command: "tmpl testing dom"
      },
      {
        summary: "Adds jest-dom to project.",
        summaryShort: "Adds jest-dom to project",
        command: "tmpl testing jest-dom"
      },
      {
        summary: "Creates location test helpers.",
        summaryShort: "Creates location test helpers",
        command: "tmpl testing location"
      },
      {
        summary: "Creates stub files for test runner.",
        summaryShort: "Creates stub files for test runner",
        command: "tmpl testing stubs"
      }
    ]
  };

  // OCLIF readme gen
  static description = Testing.docs.descriptionShort;
  static examples = docCommandsToExamples(Testing.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    try {
      await Testing.command(this.parse(Testing));
    } catch (error) {
      console.log(error.message);
      this._help();
    }
  }

  static async command({ args, flags }: ICommand) {
    switch (args.command) {
      case "device":
        await Testing.deviceAction(flags);
        break;
      case "dom":
        await Testing.domAction(flags);
        break;
      case "jest-dom":
        await Testing.jestDomAction(flags);
        break;
      case "location":
        await Testing.locationAction(flags);
        break;
      case "stubs":
        await Testing.stubsAction(flags);
        break;
      default:
        throw new Error("No matching command was provided");
    }
  }

  private static async deviceAction(flags: IFlags) {
    const typeDir = isTypescriptProject() ? "typescript" : "javascript";
    const files = [[`/testing/device/${typeDir}`, "/src/testing"]];
    await install(files, [], [], flags);
  }

  private static async domAction(flags: IFlags) {
    const typeDir = isTypescriptProject() ? "typescript" : "javascript";
    const files = [[`/testing/dom/${typeDir}`, "/src/testing"]];
    await install(files, [], [], flags);
  }

  private static async jestDomAction(flags: IFlags) {
    const devPackages = [PACKAGE.testingLibraryJestDom];
    await install([], devPackages, [], flags);
    const content = `require("@testing-library/jest-dom");`;
    prepend("./jest.setup.js", content);
  }

  private static async locationAction(flags: IFlags) {
    const typeDir = isTypescriptProject() ? "typescript" : "javascript";
    const files = [[`/testing/location/${typeDir}`, "/src/testing"]];
    await install(files, [], [], flags);
  }

  private static async stubsAction(flags: IFlags) {
    const typeDir = isTypescriptProject() ? "typescript" : "javascript";
    const files = [[`/testing/stubs/${typeDir}`, "/src/testing/stubs"]];
    await install(files, [], [], flags);
  }
}
