import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { installFlag, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { ICommand, IDocs, IFlags } from "types";

export default class Style extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for Style templates",
    commands: [
      {
        summary: "Creates a css colors file.",
        summaryShort: "Creates a css colors file",
        command: "tmpl style colors"
      },
      {
        summary: "Creates a css colors-tailwind file.",
        summaryShort: "Creates a css colors-tailwind file",
        command: "tmpl style colors-tailwind"
      },
      {
        summary: "Creates a css reset file.",
        summaryShort: "Creates a css reset file",
        command: "tmpl style reset"
      },
      {
        summary: "Creates a css file to import sakura.",
        summaryShort: "Creates a css file to import sakura",
        command: "tmpl style sakura"
      },
      {
        summary: "Creates a css file to import water.",
        summaryShort: "Creates a css file to import water",
        command: "tmpl style water"
      }
    ]
  };

  // OCLIF readme gen
  static description = Style.docs.descriptionShort;
  static examples = docCommandsToExamples(Style.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    try {
      await Style.command(this.parse(Style));
    } catch (error) {
      console.log(error.message);
      this._help();
    }
  }

  static async command({ args, flags }: ICommand) {
    switch (args.command) {
      case "breakpoints":
        await Style.breakpointsAction(flags);
        break;
      case "colors":
        await Style.colorsAction(flags);
        break;
      case "colors-tw":
      case "colors-tailwind":
        await Style.colorsTailwindAction(flags);
        break;
      case "reset":
        await Style.resetAction(flags);
        break;
      case "sakura":
        await Style.sakuraAction(flags);
        break;
      case "water":
        await Style.waterAction(flags);
        break;
      default:
        throw new Error("No matching command was provided");
    }
  }

  private static async breakpointsAction(flags: IFlags) {
    const files = [["/style/breakpoints", "."]];
    await install(files, [], [], flags);
  }

  private static async colorsAction(flags: IFlags) {
    const files = [["/style/colors", "."]];
    await install(files, [], [], flags);
  }

  private static async colorsTailwindAction(flags: IFlags) {
    const files = [["/style/colorsTailwind", "."]];
    await install(files, [], [], flags);
  }

  private static async resetAction(flags: IFlags) {
    const files = [["/style/reset", "."]];
    await install(files, [], [], flags);
  }

  private static async sakuraAction(flags: IFlags) {
    const files = [["/style/sakura", "."]];
    await install(files, [], [], flags);
  }

  private static async waterAction(flags: IFlags) {
    const files = [["/style/water", "."]];
    await install(files, [], [], flags);
  }
}
