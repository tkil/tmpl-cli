import { Command } from "@oclif/command";
import { prompt } from "inquirer";

import { install } from "@engine/installer";
import { generateBabel } from "@generators/babel/generate";
import { generateJest } from "@generators/jest/generate";
import { generateWebpack } from "@generators/webpack/generate";
import { installFlag, javascriptFlags, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { ICommand, IDocs, IFlags } from "types";
import { isTypescriptProject } from "@utils/detection";
import { resolvePath } from "@utils/pathing";
import { PACKAGE } from "@packages";

interface INodeOptions {
  isJest: boolean;
  isTypescript: boolean;
}

export default class Node extends Command {
  static docs: IDocs = {
    description: "Sub cli for Node project generation.",
    descriptionShort: "Sub cli for Node project generation",
    commands: [
      {
        summary: "Initialize a node project.",
        summaryShort: "Initialize a node project",
        command: "tmpl node init"
      },
      {
        summary: "Initialize a node project.  " + "Force Typescript",
        summaryShort: "Initialize a node project",
        command: "tmpl node init -t"
      }
    ]
  };

  // OCLIF readme gen
  static description = Node.docs.descriptionShort;
  static examples = docCommandsToExamples(Node.docs.commands);

  static flags = {
    ...installFlag,
    ...javascriptFlags,
    ...standardFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    await Node.command(this.parse(Node));
  }

  static async command({ args, flags }: ICommand) {
    switch (args.command) {
      case "module":
        await Node.moduleAction(args.arg, flags);
        return;
      default:
        const projectName = args.arg || ".";
        const options: INodeOptions = await Node.initializePrompt(projectName);
        await Node.initializeAction(projectName, options, flags);
    }
  }

  private static async initializePrompt(projectName: string) {
    if (process.env.IS_AUTOMATED === "true") {
      return {
        isJest: false,
        isNode: true,
        isTypescript: false
      };
    }
    const { options } = await prompt({
      type: "checkbox",
      message: "Please select your options",
      name: "options",
      choices: [
        {
          name: "Typescript",
          value: "typescript"
        },
        {
          name: "Jest",
          value: "jest"
        }
      ]
    });
    return {
      isJest: options.indexOf("jest") > -1,
      isNode: true,
      isTypescript: options.indexOf("typescript") > -1
    };
  }

  private static async initializeAction(path: string, opt: INodeOptions, flags: IFlags) {
    opt = Node.resolveFlagsToOptions(flags, opt);
    // Files
    let files = [];
    files.push(opt.isTypescript ? ["/packageJson/node/typescript", path] : ["/packageJson/node/javascript", path]);
    files.push(opt.isTypescript ? ["/node/init/typescript", path] : ["/node/init/javascript", path]);

    // Packages
    let devPackages: string[] = [];
    let prodPackages: string[] = [];
    if (opt.isTypescript) {
      devPackages.push(PACKAGE.tsNode);
      devPackages.push(PACKAGE.typesNode);
    } else {
      devPackages.push(PACKAGE.babelNode);
    }

    // Generators
    const webpackGen = generateWebpack(opt, path);
    files = [...files, ...webpackGen.files];
    devPackages = [...devPackages, ...webpackGen.devPackages];
    prodPackages = [...prodPackages, ...webpackGen.prodPackages];
    if (!opt.isTypescript) {
      const babelGen = generateBabel({}, path);
      files = [...files, ...babelGen.files];
      devPackages = [...devPackages, ...babelGen.devPackages];
      prodPackages = [...prodPackages, ...babelGen.prodPackages];
    }
    if (opt.isJest) {
      const jestGen = generateJest(opt, path);
      files = [...files, ...jestGen.files];
      devPackages = [...devPackages, ...jestGen.devPackages];
      prodPackages = [...prodPackages, ...jestGen.prodPackages];
    }

    // Install
    await install(files, devPackages, prodPackages, flags, path);
  }

  private static async moduleAction(nameArg: string, flags: IFlags) {
    // Validation
    if (!nameArg) {
      console.info("You must provide the name of the module");
      return;
    }

    const { name, dirPath } = resolvePath("modules", nameArg);

    // Files
    const stampVars = {
      name,
      extension: isTypescriptProject(flags) ? "ts" : "js"
    };
    const files = [["/node/module", dirPath, stampVars]];
    // Install
    await install(files, [], [], flags);
  }

  private static resolveFlagsToOptions = (flags: IFlags, opt: INodeOptions) => {
    return { ...opt, isTypescript: opt.isTypescript || Boolean(flags.typescript) };
  };
}
