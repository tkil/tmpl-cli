import { Command } from "@oclif/command";
import { exec } from "child_process";
import { promisify } from "util";

import { install } from "@engine/installer";
import { installFlag, standardFlags } from "../utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { ICommand, IDocs, IFlags } from "types";

export default class Git extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub Cli to Create gitignores and init commits",
    commands: [
      {
        summary: "Creates a general .gitignore and inits",
        summaryShort: "Creates a general .gitignore and inits",
        command: "tmpl git"
      },
      {
        summary: "Creates a csharp .gitignore and inits",
        summaryShort: "Creates a csharp .gitignore and inits",
        command: "tmpl git csharp"
      }
    ]
  };

  // OCLIF readme gen
  static description = Git.docs.descriptionShort;
  static examples = docCommandsToExamples(Git.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags
  };

  static args = [{ name: "command" }];

  async run() {
    Git.command(this.parse(Git));
  }

  static async command({ args, flags }: ICommand) {
    switch (args.command) {
      case "csharp":
        await Git.csharpAction(flags);
        break;
      default:
        await Git.generalAction(flags);
        break;
    }
  }

  private static async csharpAction(flags: IFlags) {
    // Files
    const files = [["/git/csharp", "."]];
    // Install
    await install(files, [], [], flags);
    await Git.gitInit();
  }

  private static async generalAction(flags: IFlags) {
    // Files
    const files = [["/git/general", "."]];
    // Install
    await install(files, [], [], flags);
    await Git.gitInit();
  }

  private static async gitInit() {
    await promisify(exec)(`git init && git add . && git commit -m "Initial commit"`);
  }
}
