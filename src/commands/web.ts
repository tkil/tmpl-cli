import { Command } from "@oclif/command";
import { prompt } from "inquirer";

import { install } from "@engine/installer";
import { generateJest } from "@generators/jest/generate";
import { generateBabel } from "@generators/babel/generate";
import { generateWebpack } from "@generators/webpack/generate";
import { standardFlags, installFlag, javascriptFlags } from "@utils/flags";
import { docCommandsToExamples, toKebabCase } from "@utils/format";
import { IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

interface IWebOptions {
  isJest: boolean;
  isReact: boolean;
  isSass: boolean;
  isTypescript: boolean;
}

export default class Web extends Command {
  static docs: IDocs = {
    description: "Sub cli for vanilla web projects",
    descriptionShort: "Sub cli for vanilla web projects",
    commands: [
      {
        summary: "Inits new web project.",
        summaryShort: "Inits new web project",
        command: "tmpl web init"
      },
      {
        summary: "Creates a web component.",
        summaryShort: "Creates a web component",
        command: "tmpl web component [name]"
      }
    ]
  };

  // OCLIF readme gen
  static description = Web.docs.descriptionShort;
  static examples = docCommandsToExamples(Web.docs.commands);

  static flags = {
    ...standardFlags,
    ...installFlag,
    ...javascriptFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { args, flags } = this.parse(Web);
    let options: IWebOptions = {
      isJest: false,
      isReact: false,
      isSass: false,
      isTypescript: false
    };
    switch (args.command) {
      case "init":
        options = await this.initializePrompt(".");
        await this.initializeAction(".", options, flags);
        break;
      case "component":
        await this.componentAction(args.arg, flags);
        break;
      default:
        this.log("No subcommand was provided");
    }
  }

  private async initializePrompt(projectName: string) {
    if (process.env.IS_AUTOMATED === "true") {
      return {
        isJest: false,
        isReact: false,
        isSass: false,
        isTypescript: false
      };
    }
    const { options } = await prompt({
      type: "checkbox",
      message: "Please select your options",
      name: "options",
      choices: [
        {
          name: "Typescript",
          value: "typescript"
        },
        {
          name: "SCSS",
          value: "sass"
        },
        {
          name: "Jest",
          value: "jest"
        }
      ]
    });
    return {
      isJest: options.indexOf("jest") > -1,
      isReact: false,
      isSass: options.indexOf("sass") > -1,
      isTypescript: options.indexOf("typescript") > -1
    };
  }

  private resolveFlagsToOptions = (flags: IFlags, opt: IWebOptions) => {
    return { ...opt, isTypescript: Boolean(flags.typescript) };
  };

  private async initializeAction(path: string, opt: IWebOptions, flags: IFlags) {
    opt = this.resolveFlagsToOptions(flags, opt);
    // Files
    let files: any[] = [];
    files.push(["/packageJson/web", path]);
    files.push(["/web/html", path]);
    files.push(["/style/init", path]);
    files.push(["/style/reset", path]);
    if (opt.isTypescript) {
      files.push(["/web/typescript", path]);
    } else {
      files.push(["/web/javascript", path]);
    }

    // Packages
    let devPackages: string[] = [];
    let prodPackages: string[] = [];

    // Generators
    const webpackGen = await generateWebpack(opt, path);
    files = [...files, ...webpackGen.files];
    devPackages = [...devPackages, ...webpackGen.devPackages];
    prodPackages = [...prodPackages, ...webpackGen.prodPackages];
    if (!opt.isTypescript) {
      const babelGen = await generateBabel(opt, path);
      files = [...files, ...babelGen.files];
      devPackages = [...devPackages, ...babelGen.devPackages];
      prodPackages = [...prodPackages, ...babelGen.prodPackages];
    }
    if (opt.isJest) {
      const jestGen = await generateJest(opt, path);
      files = [...files, ...jestGen.files];
      devPackages = [...devPackages, ...jestGen.devPackages];
      prodPackages = [...prodPackages, ...jestGen.prodPackages];
    }

    // Install
    await install(files, devPackages, prodPackages, flags, path);
  }

  private async componentAction(name: string, flags: IFlags) {
    let nameKebabCase = toKebabCase(name);
    // Validation
    if (!name) {
      this.log("You must provide the name of the component");
      return;
    }
    if (nameKebabCase.split("-").length < 2) {
      this.log("Web components must have a two part name, adding suffix");
      name += "Component";
      nameKebabCase += "-component";
    }

    // Files
    const stampVars = {
      name,
      nameKebabCase
    };
    const files = [["/web/component/javascript", "./src/elements", stampVars]];

    // Packages
    const prodPackages = [PACKAGE.litHtml];

    // Install
    await install(files, [], prodPackages, flags);
  }
}
