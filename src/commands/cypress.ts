import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { installFlag, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { ICommand, IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

export default class Cypress extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for Cypress templates",
    commands: [
      {
        summary: "Inits Cypress.",
        summaryShort: "Inits Cypress",
        command: "tmpl cypress init"
      },
      {
        summary: "Adds axe A11y testing config and file.",
        summaryShort: "Adds axe A11y",
        command: "tmpl cypress axe"
      }
    ]
  };

  // OCLIF readme gen
  static description = Cypress.docs.descriptionShort;
  static examples = docCommandsToExamples(Cypress.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    await Cypress.command(this.parse(Cypress));
  }

  static async command({ args, flags }: ICommand) {
    switch (args.command) {
      case "init":
        await Cypress.initializeAction(flags);
        break;
      case "axe":
        await Cypress.axeAction(flags);
        break;
      default:
        console.log("No matching sub command was provided");
    }
  }

  private static async initializeAction(flags: IFlags) {
    // Files
    const files = [["/cypress/init", "."]];
    // Packages
    let devPackages = [PACKAGE.cypress, PACKAGE.concurrently];
    // Install
    await install(files, devPackages, [], flags);
  }

  private static async axeAction(flags: IFlags) {
    // Files
    const files = [["/cypress/axe", "."]];
    // Packages
    let devPackages = [PACKAGE.axeCore, PACKAGE.cypressAxe];
    // Install
    await install(files, devPackages, [], flags);
  }
}
