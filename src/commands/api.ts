import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { isTypescriptProject } from "@utils/detection";
import { installFlag, javascriptFlags, standardFlags } from "@utils/flags";
import { docCommandsToExamples, toProperCase } from "@utils/format";
import { ICommand, IFlags, IDocs } from "types";
import { resolvePath } from "@utils/pathing";
import { PACKAGE } from "@packages";

export default class Api extends Command {
  static docs: IDocs = {
    description: "Sub CLI for api generator. " + "Creates fetch calls with a try / catch wrapper.",
    descriptionShort: "Sub cli for api generator",
    commands: [
      {
        summary:
          "Creates an interface for talking with a rest endpoint. " +
          "It will generate a file that uses fetch with a try/catch wrapper, schema validation, logging and unit test boiler plate.",
        summaryShort: "Create an api call helper",
        command: "tmpl api [name]"
      },
      {
        summary:
          "Creates an interface for talking with a rest endpoint. " +
          "It will generate a file that uses fetch with a try/catch wrapper, schema validation and logging.  " +
          "Force Typescript.",
        summaryShort: "Create an api call helper, force ts",
        command: "tmpl api [name] -t"
      }
    ]
  };

  // OCLIF readme gen
  static description = Api.docs.descriptionShort;
  static examples = docCommandsToExamples(Api.docs.commands);

  static flags = {
    ...standardFlags,
    ...javascriptFlags,
    ...installFlag
  };

  static args = [{ name: "name" }];

  async run() {
    await Api.command(this.parse(Api));
  }

  static async command({ args, flags }: ICommand) {
    if (args.name) {
      await Api.action(args.name, flags);
      return;
    }
    console.info("You must provide the name of the api");
  }

  private static async action(nameArg: string, flags: IFlags) {
    const isTypescript = isTypescriptProject(flags);
    const { name, dirPath } = resolvePath("api", nameArg);

    // Files
    const stampVars = {
      name,
      nameProperCase: toProperCase(name)
    };
    const files = [
      isTypescript // no-prettier
        ? ["/api/typescript", dirPath, stampVars]
        : ["/api/javascript", dirPath, stampVars]
    ];

    // Packages
    let devPackages = [PACKAGE.nock, PACKAGE.nodeFetch];
    if (isTypescript) {
      devPackages = [...devPackages, PACKAGE.typesNodeFetch];
    }

    // Install
    await install(files, devPackages, [], flags);
  }
}
