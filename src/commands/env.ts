import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { isTypescriptProject } from "@utils/detection";
import { standardFlags, installFlag } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { ICommand, IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

export default class Env extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Sub cli for env generator",
    commands: [
      {
        summary: "Sub CLI for env generator. " + "Creates env files under /env with a config.",
        summaryShort: "Create env files under /env with config",
        command: "tmpl env"
      }
    ]
  };

  // OCLIF readme gen
  static description = Env.docs.descriptionShort;
  static examples = docCommandsToExamples(Env.docs.commands);

  static flags = {
    ...standardFlags,
    ...installFlag
  };

  static args = [{ name: "name" }];

  async run() {
    await Env.command(this.parse(Env));
  }

  static async command({ flags }: ICommand) {
    await Env.action(flags);
  }

  private static async action(flags: IFlags) {
    // Files
    const files = [];
    files.push(["/env/core", "./env"]);
    if (isTypescriptProject()) {
      files.push(["/env/typescript", "./env"]);
    }

    // Packages
    const devPackages = [PACKAGE.dotenv, PACKAGE.dotenvWebpack];

    // Install
    await install(files, devPackages, [], flags);
  }
}
