import { Command, flags } from "@oclif/command";
import { prompt } from "inquirer";

import { PACKAGE } from "@packages";
import { install } from "@engine/installer";
import { generateReactFcVars } from "@generators/react/fc/generate";
import { generateJest } from "@generators/jest/generate";
import { generateBabel } from "@generators/babel/generate";
import { generateWebpack } from "@generators/webpack/generate";
import { isTypescriptProject } from "@utils/detection";
import { javascriptFlags, installFlag, standardFlags } from "@utils/flags";
import { toProperCase } from "@utils/format";
import { docCommandsToExamples } from "@utils/format";
import { IArgs, IDocs, IFlags } from "types";

interface IReactOptions {
  conntextName?: string;
  isJest?: boolean;
  isReact?: boolean;
  isSass?: boolean;
  isTypescript?: boolean;
  install?: boolean;
}

interface IReactFlags extends IFlags {
  contextName?: string;
  mobxName?: string;
  isQuery: boolean;
}

export default class React extends Command {
  static docs: IDocs = {
    description: "Sub cli for React project init and generators",
    descriptionShort: "Sub cli for React project init and generators",
    commands: [
      {
        summary: "Create a new React project.",
        summaryShort: "Create a new React project",
        command: "tmpl react init"
      },
      {
        summary: "Create a functional component.",
        summaryShort: "Create a functional component",
        command: "tmpl react fc [name]"
      },
      {
        summary: "Create a functional component.  Force Typescript",
        summaryShort: "Create a functional component, force Typescript",
        command: "tmpl react fc [name] -t"
      },
      {
        summary: "Create a functional component with context connected.",
        summaryShort: "Create a functional component with context connected",
        command: "tmpl react fc [name] -c contextName"
      },
      {
        summary: "Create a functional component with mobx store connection.",
        summaryShort: "Create a functional component with mobx store connection",
        command: "tmpl react fc [name] -m mobxStoreName"
      },
      {
        summary: "Create a react data context under src/stores.",
        summaryShort: "Create a react data context under src/stores",
        command: "tmpl react context [name]"
      },
      {
        summary: "Create a mobx state tree model under src/stores.",
        summaryShort: "Create a mobx state tree model under src/stores",
        command: "tmpl react mst [name]"
      }
    ]
  };

  // OCLIF readme gen
  static description = React.docs.descriptionShort;
  static examples = docCommandsToExamples(React.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags,
    ...javascriptFlags,
    contextName: flags.string({ char: "c" }),
    mobxName: flags.string({ char: "m" }),
    isQuery: flags.boolean({ char: "q" })
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    await React.command(this.parse(React));
  }

  static async command({ args, flags }: { args: IArgs; flags: IReactFlags }) {
    switch (args.command) {
      case "init":
        await React.initialize(args.arg || ".", flags);
        break;
      case "context":
        await React.contextAction(args.arg, flags);
        break;
      case "fc":
        await React.fcAction(args.arg, flags, {
          contextName: flags.contextName || "",
          mobxName: flags.mobxName || ""
        });
        break;
      case "mst":
        await React.mstAction(args.arg, flags || "");
        break;
      case "test":
        await React.testAction(args.arg, flags);
        break;
      default:
        console.info("Missing / invalid subcommand");
    }
  }

  private static async initialize(projectName: string, flags: IFlags) {
    if (!projectName) {
      console.info("Project name was not provided");
      return;
    }
    const options = await React.initializePrompt(projectName);
    await React.initializeAction(projectName, options, flags);
  }

  private static async initializePrompt(projectName: string) {
    if (process.env.IS_AUTOMATED === "true") {
      return {
        isJest: false,
        isSass: false,
        isReact: true,
        isTypescript: false
      };
    }

    const { options } = await prompt({
      type: "checkbox",
      message: "Please select your options",
      name: "options",
      choices: [
        {
          name: "Typescript",
          value: "typescript"
        },
        {
          name: "SCSS",
          value: "sass"
        },
        {
          name: "Jest",
          value: "jest"
        }
      ]
    });
    return {
      isJest: options.indexOf("jest") > -1,
      isSass: options.indexOf("sass") > -1,
      isReact: true,
      isTypescript: options.indexOf("typescript") > -1
    };
  }

  private static resolveFlagsToOptions = (flags: IFlags, opt: IReactOptions) => {
    return { ...opt, isTypescript: opt.isTypescript || Boolean(flags.typescript) };
  };

  private static async initializeAction(projectName: string, opt: IReactOptions, flags: IFlags) {
    opt = React.resolveFlagsToOptions(flags, opt);
    // Files
    let files = [];
    files.push(["/packageJson/web", projectName]);
    files.push(["/react/init/core", projectName]);
    if (opt.isTypescript) {
      files.push(["/react/init/typescript/", projectName]);
    } else {
      files.push(["/react/init/javascript/", projectName]);
    }
    files.push(["/style/init", projectName]);
    files.push(["/style/reset", projectName]);

    // Packages
    let devPackages = [PACKAGE.testingLibraryReact];
    let prodPackages = [PACKAGE.react, PACKAGE.reactDom];
    if (opt.isTypescript) {
      devPackages = [...devPackages, PACKAGE.typesReact, PACKAGE.typesReactDom];
    }

    // Generators
    const webpackGen = await generateWebpack(opt, projectName);
    files = [...files, ...webpackGen.files];
    devPackages = [...devPackages, ...webpackGen.devPackages];
    prodPackages = [...prodPackages, ...webpackGen.prodPackages];
    if (!opt.isTypescript) {
      const babelGen = await generateBabel(opt, projectName);
      files = [...files, ...babelGen.files];
      devPackages = [...devPackages, ...babelGen.devPackages];
      prodPackages = [...prodPackages, ...babelGen.prodPackages];
    }
    if (opt.isJest) {
      const jestGen = await generateJest(opt, projectName);
      files = [...files, ...jestGen.files];
      devPackages = [...devPackages, ...jestGen.devPackages];
      prodPackages = [...prodPackages, ...jestGen.prodPackages];
    }

    // Install
    await install(files, devPackages, prodPackages, flags, projectName);
  }

  private static async contextAction(name: string, flags: IFlags) {
    // Validation
    if (!name) {
      console.info("You must provide the name of the context");
      return;
    }

    // Files
    const stampVars = {
      name,
      nameProperCase: toProperCase(name)
    };
    const files = [
      isTypescriptProject(flags)
        ? ["/react/context/typescript", "/src/stores", stampVars]
        : ["/react/context/javascript", "/src/stores", stampVars]
    ];

    // Install
    await install(files, [], [], flags);
  }

  private static async fcAction(
    name: string,
    flags: IReactFlags,
    { contextName, mobxName }: { [key: string]: string }
  ) {
    // Validation
    if (!name) {
      console.info("You must provide the name of the component");
      return;
    }

    // Files
    const stampVars = await generateReactFcVars({
      name,
      contextName: contextName,
      mobxName: mobxName,
      isTypescript: isTypescriptProject(flags),
      isQuery: flags.isQuery
    });
    const files = [["/react/fc/core", "./src/components", stampVars]];
    if (flags.isQuery) {
      files.push(["/react/fc/query", "./src/components", stampVars]);
    }

    // Install
    await install(files, [], [], flags);
  }

  private static async mstAction(name: string, flags: IFlags) {
    // Validation
    if (!name) {
      console.info("You must provide the name of the mobx state tree model");
      return;
    }

    // Files
    const stampVars = {
      name,
      nameProperCase: toProperCase(name)
    };
    const files = [
      isTypescriptProject(flags)
        ? ["/react/mobxStateTree/typescript", "/src/stores", stampVars]
        : ["/react/mobxStateTree/javascript", "/src/stores", stampVars]
    ];

    // Packages
    let prodPackages = [PACKAGE.mobx, PACKAGE.mobxReact, PACKAGE.mobxStateTree];

    // Install
    await install(files, [], prodPackages, flags);
  }

  private static async testAction(name: string, flags: IReactFlags) {
    // Validation
    if (!name) {
      console.info("You must provide the name of the component");
      return;
    }

    // Files
    const stampVars = await generateReactFcVars({
      name,
      contextName: "",
      mobxName: "",
      isTypescript: isTypescriptProject(flags),
      isQuery: flags.isQuery
    });
    const files = [["/react/test/", "./src/components", stampVars]];

    // Install
    await install(files, [], [], flags);
  }
}
