import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { standardFlags } from "../utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";

interface ICommand {
  flags: IFlags;
}

export default class Lisp extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Creates a small Lisp starter project",
    commands: [
      {
        summary: "Creates a small Lips starter project with a main, test file and makefile",
        summaryShort: "Creates a small Lisp starter project",
        command: "tmpl lisp"
      }
    ]
  };

  // OCLIF readme gen
  static description = Lisp.docs.descriptionShort;
  static examples = docCommandsToExamples(Lisp.docs.commands);

  static flags = {
    ...standardFlags
  };

  async run() {
    Lisp.command(this.parse(Lisp));
  }

  static async command({ flags }: ICommand) {
    await this.action(flags);
  }

  private static async action(flags: IFlags) {
    // Files
    const files = [["/lisp", "."]];
    // Install
    await install(files, [], [], flags);
  }
}
