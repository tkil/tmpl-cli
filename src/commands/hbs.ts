import { Command } from "@oclif/command";
import { prompt } from "inquirer";

import { install } from "@engine/installer";
import { generateJest } from "@generators/jest/generate";
import { generateBabel } from "@generators/babel/generate";
import { generateWebpack } from "@generators/webpack/generate";
import { standardFlags, installFlag, javascriptFlags } from "@utils/flags";
import { docCommandsToExamples, toKebabCase } from "@utils/format";
import { IDocs, IFlags } from "types";

interface IHbsOptions {
  isHbs: boolean;
  isJest: boolean;
  isReact: boolean;
  isSass: boolean;
  isTypescript: boolean;
}

export default class Hbs extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Creates a new hbs project",
    commands: [
      {
        summary: "Inits new hbs project.",
        summaryShort: "Inits new hbs project",
        command: "tmpl hbs init"
      }
    ]
  };

  // OCLIF readme gen
  static description = Hbs.docs.descriptionShort;
  static examples = docCommandsToExamples(Hbs.docs.commands);

  static flags = {
    ...standardFlags,
    ...installFlag,
    ...javascriptFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { args, flags } = this.parse(Hbs);
    switch (args.command) {
      case "init":
        const options: IHbsOptions = await this.initializePrompt(".");
        await this.initializeAction(".", options, flags);
        break;
      default:
        this.log("No subcommand was provided");
    }
  }

  private async initializePrompt(projectName: string) {
    if (process.env.IS_AUTOMATED === "true") {
      return {
        isHbs: true,
        isJest: false,
        isReact: false,
        isSass: false,
        isTypescript: false
      };
    }
    const { options } = await prompt({
      type: "checkbox",
      message: "Please select your options",
      name: "options",
      choices: [
        {
          name: "Typescript",
          value: "typescript"
        },
        {
          name: "SCSS",
          value: "sass"
        },
        {
          name: "Jest",
          value: "jest"
        }
      ]
    });
    return {
      isHbs: true,
      isJest: options.indexOf("jest") > -1,
      isReact: false,
      isSass: options.indexOf("sass") > -1,
      isTypescript: options.indexOf("typescript") > -1
    };
  }

  private resolveFlagsToOptions = (flags: IFlags, opt: IHbsOptions) => {
    return { ...opt, isTypescript: Boolean(flags.typescript) };
  };

  private async initializeAction(path: string, opt: IHbsOptions, flags: IFlags) {
    opt = this.resolveFlagsToOptions(flags, opt);
    // Files
    let files: any[] = [];
    files.push(["/packageJson/web", path]);
    files.push(["/hbs/init", path]);
    files.push(["/style/init", path]);
    files.push(["/style/reset", path]);
    if (opt.isTypescript) {
      files.push(["/web/typescript", path]);
    } else {
      files.push(["/web/javascript", path]);
    }

    // Packages
    let devPackages: string[] = [];
    let prodPackages: string[] = [];

    // Generators
    const webpackGen = await generateWebpack(opt, path);
    files = [...files, ...webpackGen.files];
    devPackages = [...devPackages, ...webpackGen.devPackages];
    prodPackages = [...prodPackages, ...webpackGen.prodPackages];
    if (!opt.isTypescript) {
      const babelGen = await generateBabel(opt, path);
      files = [...files, ...babelGen.files];
      devPackages = [...devPackages, ...babelGen.devPackages];
      prodPackages = [...prodPackages, ...babelGen.prodPackages];
    }
    if (opt.isJest) {
      const jestGen = await generateJest(opt, path);
      files = [...files, ...jestGen.files];
      devPackages = [...devPackages, ...jestGen.devPackages];
      prodPackages = [...prodPackages, ...jestGen.prodPackages];
    }

    // Install
    await install(files, devPackages, prodPackages, flags, path);
  }
}
