import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { installFlag, standardFlags } from "../utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";

interface ICommand {
  flags: IFlags;
}

export default class Readme extends Command {
  static docs: IDocs = {
    description: "",
    descriptionShort: "Creates a README.md file in the current directory",
    commands: [
      {
        summary: "Creates a README.md file in the current directory",
        summaryShort: "Creates a README.md file in the current directory",
        command: "tmpl readme"
      }
    ]
  };

  // OCLIF readme gen
  static description = Readme.docs.descriptionShort;
  static examples = docCommandsToExamples(Readme.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags
  };

  async run() {
    Readme.command(this.parse(Readme));
  }

  static async command({ flags }: ICommand) {
    await this.action(flags);
  }

  private static async action(flags: IFlags) {
    // Files
    const files = [["/readme", "."]];
    // Install
    await install(files, [], [], flags);
  }
}
