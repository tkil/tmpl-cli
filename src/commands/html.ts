import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { resolvePath } from "@utils/pathing";
import { ICommand, IDocs, IFlags } from "types";

export default class Html extends Command {
  static docs: IDocs = {
    description: "Sub cli for html templates, files are added to src/",
    descriptionShort: "Sub cli for html templates",
    commands: [
      {
        summary: "Adds a simple html form file.",
        summaryShort: "Adds a simple html form file",
        command: "tmpl html form [name]"
      }
    ]
  };

  // OCLIF readme gen
  static description = Html.docs.descriptionShort;
  static examples = docCommandsToExamples(Html.docs.commands);

  static flags = {
    ...standardFlags
  };

  static args = [{ name: "command" }, { name: "name" }];

  async run() {
    await Html.command(this.parse(Html));
  }

  static async command({ args, flags }: ICommand) {
    switch (args.command) {
      case "form":
        await Html.formAction(args.name, flags);
        break;
      default:
        console.info("No subcommand was provided");
    }
  }

  private static async formAction(nameArg: string, flags: IFlags) {
    // Validation
    if (!nameArg) {
      console.info("You must provide the name of the html form file");
      return;
    }

    const { name, dirPath } = resolvePath(".", nameArg);

    // Files
    const stampVars = {
      name
    };
    const files = [["/html/form", dirPath, stampVars]];

    // Install
    await install(files, [], [], flags);
  }
}
