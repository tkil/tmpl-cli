import { Command } from "@oclif/command";

import { install } from "@engine/installer";
import { isTypescriptProject } from "@utils/detection";
import { installFlag, javascriptFlags, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IDocs, IFlags } from "types";
import { PACKAGE } from "@packages";

export default class Util extends Command {
  static docs: IDocs = {
    description: "Sub cli for util helps.",
    descriptionShort: "Sub cli for util helps",
    commands: [
      {
        summary: "Adds async util.",
        summaryShort: "Adds async util",
        command: "tmpl util async"
      },
      {
        summary: "Adds cookie util.",
        summaryShort: "Adds cookie util",
        command: "tmpl util cookie"
      },
      {
        summary: "Adds dynamic util.",
        summaryShort: "Adds dynamic util",
        command: "tmpl util dynamic"
      },
      {
        summary: "Adds event-listener util.",
        summaryShort: "Adds event-listener util",
        command: "tmpl util event-listener"
      },
      {
        summary: "Adds history util.",
        summaryShort: "Adds history util",
        command: "tmpl util history"
      },
      {
        summary: "Adds location util.",
        summaryShort: "Adds location util",
        command: "tmpl util location"
      },
      {
        summary: "Adds polyfills util.",
        summaryShort: "Adds polyfills util",
        command: "tmpl util polyfills"
      },
      {
        summary: "Adds react util.",
        summaryShort: "Adds react util",
        command: "tmpl util react"
      },
      {
        summary: "Adds retry util.",
        summaryShort: "Adds retry util",
        command: "tmpl util retry"
      }
    ]
  };

  // OCLIF readme gen
  static description = Util.docs.descriptionShort;
  static examples = docCommandsToExamples(Util.docs.commands);

  static flags = {
    ...standardFlags,
    ...installFlag,
    ...javascriptFlags
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    const { args, flags } = this.parse(Util);
    switch (args.command) {
      case "async":
        await this.asyncAction(flags);
        break;
      case "cookie":
        await this.cookieAction(flags);
        break;
      case "dynamic":
        await this.dynamicAction(flags);
        break;
      case "event-listener":
      case "event-listener-reg":
      case "eventlistener":
      case "eventlistenerreg":
        await this.eventListenerRegAction(flags);
        break;
      case "history":
        await this.historyAction(flags);
        break;
      case "location":
        await this.locationAction(flags);
        break;
      case "polyfills":
        await this.polyfillsAction(flags);
        break;
      case "react":
        await this.reactAction(flags);
        break;
      case "retry":
        await this.retryAction(flags);
        break;
      default:
        this.log("No subcommand was provided");
    }
  }

  private async asyncAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags) ? ["/util/async/typescript", "/src/utils"] : ["/util/async/javascript", "/src/utils"]
    ];
    // Install
    await install(files, [], [], flags);
  }

  private async cookieAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags) ? ["/util/cookie/typescript", "/src/utils"] : ["/util/cookie/javascript", "/src/utils"]
    ];
    // Install
    await install(files, [], [], flags);
  }

  private async dynamicAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags)
        ? ["/util/dynamic/typescript", "/src/utils"]
        : ["/util/dynamic/javascript", "/src/utils"]
    ];
    // Install
    await install(files, [], [], flags);
  }

  private async eventListenerRegAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags)
        ? ["/util/eventListenerReg/typescript", "/src/utils"]
        : ["/util/eventListenerReg/javascript", "/src/utils"]
    ];
    // Install
    await install(files, [], [], flags);
  }

  private async historyAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags)
        ? ["/util/history/typescript", "/src/utils"]
        : ["/util/history/javascript", "/src/utils"]
    ];
    // Install
    await install(files, [], [], flags);
  }

  private async locationAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags)
        ? ["/util/location/javascript", "/src/utils"]
        : ["/util/location/javascript", "/src/utils"]
    ];
    // Install
    await install(files, [], [], flags);
  }

  private async polyfillsAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags)
        ? ["/util/polyfills/typescript", "/src/utils"]
        : ["/util/polyfills/javascript", "/src/utils"]
    ];
    // Packages
    const prodPackages = [PACKAGE.isomorphicFetch];
    // Install
    await install(files, [], prodPackages, flags);
  }

  private async reactAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags) ? ["/util/react/typescript", "/src/utils"] : ["/util/react/javascript", "/src/utils"]
    ];
    // Packages
    let devPackages = isTypescriptProject(flags) ? [PACKAGE.typesReact, "@types/react-dom"] : ["@babel/preset-react"];
    const prodPackages = [PACKAGE.react, PACKAGE.reactDom];
    // Install
    await install(files, devPackages, prodPackages, flags);
    // Info
    if (!isTypescriptProject(flags)) {
      this.log("FYI: You must add @babel/preset-react to the babel presets");
      this.log('Example:\n{\n  "presets": ["@babel/preset-react"]\n}');
    }
  }

  private async retryAction(flags: IFlags) {
    // Files
    const files = [
      isTypescriptProject(flags) //
        ? ["/util/retry/typescript", "/src/utils"]
        : ["/util/retry/typescript", "/src/utils"]
    ];
    // Install
    await install(files, [], [], flags);
  }
}
