import { Command, flags } from "@oclif/command";
import { prompt } from "inquirer";

import { PACKAGE } from "@packages";
import { install } from "@engine/installer";
import { generateJest } from "@generators/jest/generate";
import { javascriptFlags, installFlag, standardFlags } from "@utils/flags";
import { docCommandsToExamples } from "@utils/format";
import { IArgs, IDocs, IFlags } from "types";
import { generateTsConfig } from "@generators/tsconfig/generate";

interface IGatsbyOptions {
  conntextName?: string;
  isJest?: boolean;
  isReact?: boolean;
  isTypescript?: boolean;
  install?: boolean;
}

interface IGatsbyFlags extends IFlags {
  contextName?: string;
  mobxName?: string;
}

export default class Gatsby extends Command {
  static docs: IDocs = {
    description: "Sub cli for Gatsby project init and generators",
    descriptionShort: "Sub cli for Gatsby project init and generators",
    commands: [
      {
        summary: "Create a new Gatsby project.",
        summaryShort: "Create a new Gatsby project",
        command: "tmpl react init"
      }
    ]
  };

  // OCLIF readme gen
  static description = Gatsby.docs.descriptionShort;
  static examples = docCommandsToExamples(Gatsby.docs.commands);

  static flags = {
    ...installFlag,
    ...standardFlags,
    ...javascriptFlags,
    contextName: flags.string({ char: "c" }),
    mobxName: flags.string({ char: "m" })
  };

  static args = [{ name: "command" }, { name: "arg" }];

  async run() {
    await Gatsby.command(this.parse(Gatsby));
  }

  static async command({ args, flags }: { args: IArgs; flags: IGatsbyFlags }) {
    switch (args.command) {
      case "init":
        await Gatsby.initialize(args.arg || ".", flags);
        break;
      default:
        console.info("Missing / invalid subcommand");
    }
  }

  private static async initialize(projectName: string, flags: IFlags) {
    if (!projectName) {
      console.info("Project name was not provided");
      return;
    }
    const options = await Gatsby.initializePrompt(projectName);
    await Gatsby.initializeAction(projectName, options, flags);
  }

  private static async initializePrompt(projectName: string) {
    if (process.env.IS_AUTOMATED === "true") {
      return {
        isJest: false,
        isReact: true,
        isTypescript: false
      };
    }

    const { options } = await prompt({
      type: "checkbox",
      message: "Please select your options",
      name: "options",
      choices: [
        {
          name: "Typescript",
          value: "typescript"
        },
        {
          name: "Jest",
          value: "jest"
        }
      ]
    });
    return {
      isJest: options.indexOf("jest") > -1,
      isReact: true,
      isTypescript: options.indexOf("typescript") > -1
    };
  }

  private static resolveFlagsToOptions = (flags: IFlags, opt: IGatsbyOptions) => {
    return { ...opt, isTypescript: opt.isTypescript || Boolean(flags.typescript) };
  };

  private static async initializeAction(projectPath: string, opt: IGatsbyOptions, flags: IFlags) {
    opt = Gatsby.resolveFlagsToOptions(flags, opt);
    // Files
    let files = [];
    files.push(["/packageJson/gatsby", projectPath]);
    files.push(["/gatsby/init", projectPath]);
    // files.push(["/gatsby/init/core", projectPath]);
    // if (opt.isTypescript) {
    // files.push(["/gatsby/init/typescript/", projectPath]);
    // } else {
    // files.push(["/gatsby/init/javascript/", projectPath]);
    // }
    // files.push(["/style/init", projectPath]);
    // files.push(["/style/reset", projectPath]);

    // Packages
    let devPackages: string[] = [];
    let prodPackages = [
      PACKAGE.gatsby,
      PACKAGE.gatsbyImage,
      PACKAGE.gatsbyPluginPostcss,
      PACKAGE.gatsbyPluginReactHelmet,
      PACKAGE.gatsbyPluginSharp,
      PACKAGE.gatsbySourceFilesystem,
      PACKAGE.gatsbyTransformerSharp,
      PACKAGE.postCss,
      PACKAGE.react,
      PACKAGE.reactDom,
      PACKAGE.reactHelmet
    ];
    if (opt.isTypescript) {
      devPackages = [...devPackages, PACKAGE.typescript, PACKAGE.typesReact, PACKAGE.typesReactDom];
    }

    // Generators
    if (opt.isTypescript) {
      const tsConfigGen = generateTsConfig(opt, projectPath);
      files = [...files, ...tsConfigGen.files];
      devPackages = [...devPackages, ...tsConfigGen.devPackages];
    }
    if (opt.isJest) {
      const jestGen = await generateJest(opt, projectPath);
      files = [...files, ...jestGen.files];
      devPackages = [...devPackages, ...jestGen.devPackages];
      prodPackages = [...prodPackages, ...jestGen.prodPackages];
    }

    // Install
    await install(files, devPackages, prodPackages, flags, projectPath);
  }
}
