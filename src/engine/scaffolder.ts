import * as chalk from "chalk";
import { promises as fsAsync } from "fs";
import * as rimraf from "rimraf";
import * as readDir from "recursive-readdir";

import { applyTemplate } from "@engine/templator";
import { templatingStagingDirName } from "@utils/constants";
import { copyDrill, exists, templatesDir, delay } from "@utils/io";
import { getExtension } from "@utils/format";
import { prettyFile } from "@utils/io";
import { IObjString, IFlags } from "types";

const stampTemplate = (inDir: string, outDir = ".", vars: IObjString = {}): Promise<string[]> =>
  new Promise((resolve, reject) => {
    applyTemplate(inDir, outDir, vars, (error: Error, createdFiles: string[]) => {
      if (error) {
        reject(error);
      }
      resolve(createdFiles);
    });
  });

const createStagingDir = async (projectPath = ".") => {
  try {
    await fsAsync.mkdir(`${projectPath}/${templatingStagingDirName}`);
  } catch (err) {}
};

const removeStagingDir = (projectPath = ".") =>
  new Promise(resolve => rimraf(`${projectPath}/${templatingStagingDirName}`, resolve));

const findOverwriteConflicts = async (filePaths: string[]): Promise<string[]> => {
  const existsWithHoles = await Promise.all(
    filePaths.map(async fp => {
      const target = fp.replace(`${templatingStagingDirName}`, ".");
      return (await exists(target)) ? target : undefined;
    })
  );
  return existsWithHoles.filter(fp => Boolean(fp)) as string[];
};

const stageFiles = async (stampTuple: any, destBasePath = ".") => {
  const stagingPath = `${destBasePath}/${templatingStagingDirName}`;
  for (const st of stampTuple) {
    await stampTemplate(`${templatesDir}/${st[0]}`, `${stagingPath}/${st[1]}`, st[2]);
  }
  return readDir(`${stagingPath}`);
};

const getFileSize = async (filePath: string) => {
  try {
    return (await fsAsync.stat(filePath)).size;
  } catch (err) {
    return "??";
  }
};

const copyFiles = async (stagedFiles: string[], existingFiles: string[], isDryRun: boolean) => {
  for (const fp of stagedFiles) {
    const target = fp.replace(templatingStagingDirName, ".");
    const size = await getFileSize(fp);
    if(!isDryRun) {
      await copyDrill(fp, target);
    }
    const action = existingFiles.indexOf(target) > -1 ? chalk.yellow("OVERWRITE ") : chalk.green("CREATE ");
    console.info(action + target + ` (${size} bytes)`);
  }
};

const prettyStaged = async (dirPath: string) => {
  const filePaths = await readDir(dirPath);
  for (const fp of filePaths) {
    const extensions = [".js", ".jsx", ".ts", ".tsx"];
    if (extensions.indexOf(getExtension(fp)) > -1) {
      await prettyFile(fp);
    }
  }
};

export const stageAndScaffold = async (stampTuple: any, flags: IFlags, destBasePath = ".") => {
  let isSuccess = false;
  try {
    createStagingDir(destBasePath);
    const stagedFiles = await stageFiles(stampTuple, destBasePath);
    const overwriteConflicts = await findOverwriteConflicts(stagedFiles);
    await prettyStaged(`${process.cwd()}/${templatingStagingDirName}`);
    if (overwriteConflicts.length === 0 || flags["dry-run"] || flags.force) {
      await copyFiles(stagedFiles, overwriteConflicts, Boolean(flags["dry-run"]));
      isSuccess = true;
    } else {
      console.info("Files exist, aborting templating, use the -f flag to force overwrite conflicts");
      console.info(overwriteConflicts);
    }
    if (flags["dry-run"]) {
      console.info(chalk.yellow("\nThis was a dry run. To apply the code generation, re-run without the -d flag\n"));
    }
    await removeStagingDir(destBasePath);
    return isSuccess;
  } catch (err) {}
};
