import { stageAndScaffold } from "@engine/scaffolder";
import { npmInstallDev, npmInstallProd } from "@engine/npm";
import { IFlags } from "types";

export const install = async (
  files: any[][],
  devPackages: string[],
  prodPackages: string[],
  flags: IFlags,
  path: string = "."
) => {
  const isSuccess = await stageAndScaffold(files, flags);
  if (!flags["dry-run"] && isSuccess) {
    if (prodPackages.length > 0) {
      await npmInstallProd(prodPackages, flags, path);
    }
    if (devPackages.length > 0) {
      await npmInstallDev(devPackages, flags, path);
    }
  }
};
