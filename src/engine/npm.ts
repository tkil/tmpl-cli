import cli from "cli-ux";
import { exec } from "child_process";
import { promisify } from "util";
import * as path from "path";
import { existsSync, readFileSync, writeFileSync } from "fs";
import * as latestVersion from "latest-version";

import { prettyFile } from "@utils/io";
import { IFlags } from "types";

const getPackagePath = (directory = ".") => path.join(directory, "package.json");

const filterOutInstalled = (depProperty: string, packages: string[], directory = ".") =>
  packages.filter(packageName => {
    const npmData = JSON.parse(readFileSync(getPackagePath(directory), "utf8"));
    return !Boolean(npmData[depProperty] && npmData[depProperty][packageName]);
  });

const npmInstallActive = async (depProperty: string, commandFlag: string, packages: string[], directory = ".") => {
  const packagesToInstall = filterOutInstalled(depProperty, packages, directory);
  if (packagesToInstall.length > 0) {
    await promisify(exec)(`cd ${directory} && npm install ${commandFlag} ${packagesToInstall.join(" ")}`);
    await promisify(exec)(`cd ${directory} && npm install`);
  }
};

const npmInstallPassive = async (depProperty: string, packages: string[], directory = ".") => {
  const packagePath = path.join(directory, "package.json");
  if (existsSync(packagePath)) {
    const npmData = JSON.parse(readFileSync(packagePath, "utf8"));
    const existingDependencies = npmData[depProperty] || {};
    const newDependencies = { ...existingDependencies, ...(await determineNpmVersions(packages.sort())) };
    npmData[depProperty] = newDependencies;
    writeFileSync(packagePath, JSON.stringify(npmData), "utf8");
    prettyFile(packagePath);
  }
};

export const npmInstallDev = async (packages: string[], flags: IFlags, directory = ".") => {
  cli.action.start(
    flags.install ? "Installing NPM dev dependencies..." : "Adding package dev dependencies without install..."
  );
  flags.install
    ? await npmInstallActive("devDependencies", "--save-dev", packages, directory)
    : await npmInstallPassive("devDependencies", packages, directory);
  cli.action.stop();
};

export const npmInstallProd = async (packages: string[], flags: IFlags, directory = ".") => {
  cli.action.start(flags.install ? "Installing NPM dependencies..." : "Adding package dependencies without install...");
  flags.install
    ? await npmInstallActive("dependencies", "--save", packages, directory)
    : await npmInstallPassive("dependencies", packages, directory);
  cli.action.stop();
};

interface IStrObj {
  [key: string]: string;
}
export const determineNpmVersions = async (packages: string[]) => {
  const packageVersions = await packages.reduce(async (accP, curr) => {
    const acc = await accP;
    if (curr === "babel-core@7.0.0-bridge.0") {
      acc["babel-core"] = "7.0.0-bridge.0";
    } else {
      const packageName = curr.replace(/@[0-9].*/, "");
      const desiredVersion = curr.replace(`${packageName}`, "").replace(`@`, "");
      const retrievedVersion = await latestVersion(
        packageName,
        Boolean(desiredVersion) ? { version: desiredVersion } : undefined
      );
      acc[packageName] = `^${retrievedVersion}`;
    }
    return acc;
  }, {} as Promise<IStrObj>);
  return packageVersions;
};
