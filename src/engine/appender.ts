import { existsSync, readFileSync, writeFileSync } from "fs";
import { prettyFile } from "@utils/io";

export const append = async (targetFilePath: string, content: string, isPrepend = false) => {
  if (existsSync(targetFilePath)) {
    let strData = readFileSync(targetFilePath, "utf8");
    strData = isPrepend ? `${content}\n${strData}` : `${strData}\n${content}`;
    writeFileSync(targetFilePath, strData, "utf8");
    prettyFile(targetFilePath);
  } else {
    writeFileSync(targetFilePath, `${content}\n`, "utf8");
  }
};

export const prepend = async (targetFilePath: string, content: string) => {
  append(targetFilePath, content, true);
};
