const parallel = require("run-parallel");
const eos = require("end-of-stream");
const readdirp = require("readdirp");
const assert = require("assert");
const mkdirp = require("mkdirp");
const noop = require("noop2");
const path = require("path");
const pump = require("pump");
const fs = require("graceful-fs");

const through = require("through2");
const split = require("split2");

const openShield = "_{_{_{_{";
const closeShield = "}_}_}_}_";

function shield(str: any) {
  return str.replace(new RegExp("{{{{", "g"), openShield).replace(new RegExp("}}}}", "g"), closeShield);
}

function unshield(str: any) {
  return str.replace(new RegExp(openShield, "g"), "{{").replace(new RegExp(closeShield, "g"), "}}");
}

function maxstache2(str: any, ctx: any) {
  function parse(ctx: any) {
    return function parse(token: any, i: number) {
      if (i % 2 === 0) return token;
      return ctx[token];
    };
  }

  ctx = ctx || {};
  assert.equal(typeof str, "string");
  assert.equal(typeof ctx, "object");
  const tokens = shield(str).split(/\{\{|\}\}/);
  const res = tokens.map(parse(ctx));
  return unshield(res.join(""));
}

function maxstacheStream2(args: any) {
  function parse(args: any) {
    return through(function(chunk: any, enc: any, cb: any) {
      const str = String(chunk);
      cb(null, maxstache2(str, args));
    });
  }

  args = args || {};
  assert.equal(typeof args, "object");
  return pump(split(), parse(args));
}

// write a file to a directory
// str -> stream
function writeFile(outDir: any, vars: any, file: any) {
  return function(done: any) {
    const fileName = file.path;
    const inFile = file.fullPath;
    const parentDir = file.parentDir;
    const outFile = path.join(outDir, maxstache2(removeUnderscore(fileName), vars));
    mkdirp(path.join(outDir, maxstache2(parentDir, vars)), function(err: any) {
      if (err) return done(err);

      const rs = fs.createReadStream(inFile);
      const ts = maxstacheStream2(vars);
      const ws = fs.createWriteStream(outFile);

      pump(rs, ts, ws, done);
    });
  };
}

// remove a leading underscore
// str -> str
function removeUnderscore(filepath: any) {
  const parts = filepath.split(path.sep);
  const filename = parts.pop()!.replace(/^_/, "");
  return parts.concat([filename]).join(path.sep);
}

export const applyTemplate = function(srcDir: any, outDir: any, vars: any, cb: any) {
  if (!cb) {
    if (!vars) vars = noop;
    cb = vars;
    vars = {};
  }

  assert.equal(typeof srcDir, "string");
  assert.equal(typeof outDir, "string");
  assert.equal(typeof vars, "object");
  assert.equal(typeof cb, "function");

  // create directory
  mkdirp(outDir, function(err: any) {
    if (err) return cb(err);

    const rs = readdirp({ root: srcDir });
    const streams: any = [];
    const createdFiles: any = [];

    // create a new stream for every file emitted
    rs.on("data", function(file: any) {
      createdFiles.push(path.join(outDir, maxstache2(removeUnderscore(file.path), vars)));
      streams.push(writeFile(outDir, vars, file));
    });

    // delegate errors & close streams
    eos(rs, function(err: any) {
      if (err) return cb(err);
      parallel(streams, function(err: any) {
        if (err) return cb(err);
        cb(null, createdFiles);
      });
    });
  });
};

export default applyTemplate;
